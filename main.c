// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Copyright (c) 04.2007 Holger Buss
// + Nur für den privaten Gebrauch
// + www.MikroKopter.com
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Es gilt für das gesamte Projekt (Hardware, Software, Binärfiles, Sourcecode und Dokumentation),
// + dass eine Nutzung (auch auszugsweise) nur für den privaten und nicht-kommerziellen Gebrauch zulässig ist.
// + Sollten direkte oder indirekte kommerzielle Absichten verfolgt werden, ist mit uns (info@mikrokopter.de) Kontakt
// + bzgl. der Nutzungsbedingungen aufzunehmen.
// + Eine kommerzielle Nutzung ist z.B.Verkauf von MikroKoptern, Bestückung und Verkauf von Platinen oder Bausätzen,
// + Verkauf von Luftbildaufnahmen, usw.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Werden Teile des Quellcodes (mit oder ohne Modifikation) weiterverwendet oder veröffentlicht,
// + unterliegen sie auch diesen Nutzungsbedingungen und diese Nutzungsbedingungen incl. Copyright müssen dann beiliegen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Sollte die Software (auch auszugesweise) oder sonstige Informationen des MikroKopter-Projekts
// + auf anderen Webseiten oder Medien veröffentlicht werden, muss unsere Webseite "http://www.mikrokopter.de"
// + eindeutig als Ursprung verlinkt und genannt werden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Keine Gewähr auf Fehlerfreiheit, Vollständigkeit oder Funktion
// + Benutzung auf eigene Gefahr
// + Wir übernehmen keinerlei Haftung für direkte oder indirekte Personen- oder Sachschäden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Portierung der Software (oder Teile davon) auf andere Systeme (ausser der Hardware von www.mikrokopter.de) ist nur
// + mit unserer Zustimmung zulässig
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Funktion printf_P() unterliegt ihrer eigenen Lizenz und ist hiervon nicht betroffen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Redistributions of source code (with or without modifications) must retain the above copyright notice,
// + this list of conditions and the following disclaimer.
// +   * Neither the name of the copyright holders nor the names of contributors may be used to endorse or promote products derived
// +     from this software without specific prior written permission.
// +   * The use of this project (hardware, software, binary files, sources and documentation) is only permittet
// +     for non-commercial use (directly or indirectly)
// +     Commercial use (for excample: selling of MikroKopters, selling of PCBs, assembly, ...) is only permitted
// +     with our written permission
// +   * If sources or documentations are redistributet on other webpages, out webpage (http://www.MikroKopter.de) must be
// +     clearly linked as origin
// +   * porting to systems other than hardware from www.mikrokopter.de is not allowed
// +  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// +  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// +  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// +  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// +  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// +  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// +  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// +  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// +  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// +  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// +  POSSIBILITY OF SUCH DAMAGE.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include <avr/boot.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "main.h"
#include "timer0.h"
#include "timer2.h"
#include "uart0.h"
#include "uart1.h"
#include "led.h"
#include "menu.h"
#include "fc.h"
#include "motor.h"
#include "rc.h"
#include "analog.h"
#include "printf_P.h"
#ifdef USE_KILLAGREG
#include "mm3.h"
#endif
#ifdef USE_NAVICTRL
#include "spi.h"
#endif
#ifdef USE_MK3MAG
#include "mk3mag.h"
#endif
#include "twimaster.h"
#include "eeprom.h"


uint8_t BoardRelease = 10;
CPUType_t CPUType = ATMEGA644;

/*!
	Determines the type of the CPU used.
	\note works only after reset or power on when the registers have default values
*/
CPUType_t GetCPUType(void)
{
	// TODO: determine whether the variable can be renamed for clarification that it does not overwrite the global one
	CPUType_t CPUType = ATMEGA644;
	// initial Values for 644P after reset
	if( (UCSR1A == 0x20) && (UCSR1C == 0x06) ) { // TODO: replace magic numbers
		CPUType = ATMEGA644P;
	}
	return CPUType;
}


uint8_t GetBoardRelease(void)
{
	uint8_t BoardRelease = 10;
	// the board release is coded via the pull up or down the 2 status LED

	PORTB &= ~((1 << PORTB1)|(1 << PORTB0)); // set tristate
	DDRB  &= ~((1 << DDB0)|(1 << DDB0)); // set port direction as input

	_delay_loop_2(1000); // make some delay

	switch( PINB & ((1<<PINB1)|(1<<PINB0)) )
	{
	case 0x00:
		BoardRelease = 10; // 1.0
		break;
	case 0x01:
		BoardRelease = 11; // 1.1 or 1.2
		break;
	case 0x02:
		BoardRelease = 20; // 2.0
		break;
	case 0x03:
		BoardRelease = 13; // 1.3
		break;
	default:
		break;
	}
	// set LED ports as output
	DDRB |= (1<<DDB1)|(1<<DDB0);
	LED_Red_Set(true);
	LED_Green_Set(false);
	return BoardRelease;
}

inline void Main_Init()
{
	// disable interrupts globally
	cli();

	// analyze hardware environment
	CPUType = GetCPUType();
	BoardRelease = GetBoardRelease();

	// disable watchdog
	MCUSR &=~(1<<WDRF);
	WDTCSR |= (1<<WDCE)|(1<<WDE);
	WDTCSR = 0;

	Beep_SetBeepTime(200);

	PPM_in[CH_GAS] = 0;
	StickYaw = 0;
	StickRoll = 0;
	StickPitch = 0;

	LED_Red_Set(false);

	// initalize modules
	LED_Init();
	Timer_Timer0_Init();
	TIMER2_Init();
	USART0_Init();
	if(CPUType == ATMEGA644P) {
		USART1_Init();
	}
	RC_Init();
	ADC_Init();
	TWI_Init();
	#ifdef USE_NAVICTRL
		SPI_MasterInit();
	#endif
	#ifdef USE_KILLAGREG
		MM3_Init();
	#endif
	#ifdef USE_MK3MAG
		MK3MAG_Init();
	#endif

	// enable interrupts globally
	sei();
}

inline void Main_Print()
{
	// for storing timeouts
	timeout_t timeout;

	printf("\n\r===================================");
	printf("\n\rFlightControl");
	printf("\n\rHardware: %d.%d", BoardRelease/10, BoardRelease%10);
	if(CPUType == ATMEGA644P) {
		printf("\r\n     CPU: Atmega644p");
	} else {
		printf("\r\n     CPU: Atmega644");
	}
	printf("\n\rSoftware: V%d.%d%c",VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH + 'a');
	printf("\n\r===================================");
	LED_Green_Set(true);

	// Parameter Set handling
	ParamSet_Init();

	// Check connected BL-Ctrls
	printf("\n\rFound BL-Ctrl: ");
	TWI_MotorReadIndex = 0;
	UpdateMotor = false;
	SendMotorData();
	while(!UpdateMotor);
	TWI_MotorReadIndex = 0;  // read the first I2C-Data
	uint8_t i;
	for(i = 0; i < Motor_Count; i++)
	{
		UpdateMotor = false;
		SendMotorData();
		while(!UpdateMotor);
		if(Motor[i].Present) printf("%d ",i+1);
	}
	for(i = 0; i < Motor_Count; i++)
	{
		if(!Motor[i].Present && Mixer.Motor[i][MIX_GAS] > 0) printf("\n\r\n\r!! MISSING BL-CTRL: %d !!",i+1);
		Motor[i].Error = 0;
	}
	printf("\n\r===================================");


	if(GetParamWord(PID_ACC_NICK) > 2048)
	{
		printf("\n\rACC not calibrated!");
	}

	//wait for a short time (otherwise the RC channel check won't work below)
	timeout = Timer_TimeoutIn(500);
	while(!Timer_HasTimedOut(timeout));

	if(ParamSet.GlobalConfig & CFG_HEIGHT_CONTROL)
	{
		printf("\n\rCalibrating air pressure sensor..");
		timeout = Timer_TimeoutIn(1000);
		SearchAirPressureOffset();
		while (!Timer_HasTimedOut(timeout));
		printf("OK\n\r");
	}

	#ifdef USE_NAVICTRL
		printf("\n\rSupport for NaviCtrl");
		#ifdef USE_RC_DSL
			printf("\r\nSupport for DSL RC at 2nd UART");
		#endif
		#ifdef USE_RC_SPECTRUM
			printf("\r\nSupport for SPECTRUM RC at 2nd UART");
		#endif
	#endif

	#ifdef USE_KILLAGREG
		printf("\n\rSupport for MicroMag3 Compass");
	#endif

	#ifdef USE_MK3MAG
		printf("\n\rSupport for MK3MAG Compass");
	#endif

	#if (defined (USE_KILLAGREG) || defined (USE_MK3MAG))
		if(CPUType == ATMEGA644P) printf("\n\rSupport for GPS at 2nd UART");
		else                      printf("\n\rSupport for GPS at 1st UART");
	#endif

	SetNeutral(false);

	LED_Red_Set(false);

	Beep_SetBeepTime(200);
	ExternControl.Digital[0] = 0x55;

	printf("\n\rControl: ");
	if (ParamSet.GlobalConfig & CFG_HEADING_HOLD) printf("HeadingHold");
	else printf("Neutral (ACC-Mode)");

	printf("\n\n\r");

	LCD_Clear();
}

inline void Main_Loop()
{
	// for storing timeouts
	timeout_t timeout = Timer_TimeoutIn(0);

	while (1)
	{
		if(UpdateMotor && ADReady) // control interval
		{
			UpdateMotor = false; // reset Flag, is enabled every 2 ms by ISR of Timer0

			//J4HIGH;
			MotorControl();
			//J4LOW;

			SendMotorData(); // the flight control code
			LED_Red_Set(false);

			if(PcAccess) PcAccess--;
			else
			{
				ExternControl.Config = 0;
			}
			if(RC_Quality)  RC_Quality--;

			#ifdef USE_NAVICTRL
				if(NCDataOkay)
				{
					if(--NCDataOkay == 0) // no data from NC
					{ // set gps control sticks neutral
						GPSStickPitch = 0;
						GPSStickRoll = 0;
						NCSerialDataOkay = 0;
					}
				}
			#endif

			if(!--TWI_Timeout || MissingMotor) // try to reset the i2c if motor is missing ot timeout
			{
				LED_Red_Set(true);
				if(!TWI_Timeout)
				{
					TWI_Reset();
					TWI_Timeout = 5;
				}
				if((BeepModulation == 0xFFFF) && (MKFlags & MKFLAG_MOTOR_RUN) )
				{
					Beep_SetBeepTime(1000); // 1 second
					BeepModulation = 0x0080;
				}
			}
			else
			{
				LED_Red_Set(false);
			}

			// allow Serial Data Transmit if motors must not updated or motors are not running
			if( !UpdateMotor || !(MKFlags & MKFLAG_MOTOR_RUN) )
			{
				USART0_TransmitTxData();
			}
			USART0_ProcessRxData();

			if(Timer_HasTimedOut(timeout))
			{
				if(UBat < ParamSet.LowVoltageWarning)
				{
					BeepModulation = 0x0300;
					if(!Beep_IsBeeping())
					{
						Beep_SetBeepTime(600); // 0.6 seconds
					}
				}
				#ifdef USE_NAVICTRL
					SPI_StartTransmitPacket();
					SendSPI = 4;
				#endif
				timeout = Timer_TimeoutIn(20); // every 20 ms
			}

			LED_Update();
		}

		#ifdef USE_NAVICTRL
			if(!SendSPI)
			{	// SendSPI is decremented in timer0.c with a rate of 9.765 kHz.
				// within the SPI_TransmitByte() routine the value is set to 4.
				// I.e. the SPI_TransmitByte() is called at a rate of 9.765 kHz/4= 2441.25 Hz,
				// and therefore the time of transmission of a complete spi-packet (32 bytes) is 32*4/9.765 kHz = 13.1 ms.
				SPI_TransmitByte();
			}
		#endif
	}
}

int16_t main (void)
{
	Main_Init();

	Main_Print();

	TWI_Timeout = 5000;
	Main_Loop();
	return (1);
}
