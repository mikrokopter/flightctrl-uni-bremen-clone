/*!
	\file
	\brief Containts methods and structures for dealing with EEPROM access and parameter sets.
*/

#ifndef _EEPROM_H
#define _EEPROM_H

#include <inttypes.h>

#define EEPROM_ADR_PARAM_BEGIN	0
#define PID_PARAM_REVISION   1 // byte
#define PID_ACTIVE_SET       2 // byte
#define PID_PRESSURE_OFFSET  3 // byte
#define PID_ACC_NICK         4 // word
#define PID_ACC_ROLL         6 // word
#define PID_ACC_TOP          8 // word

#ifdef USE_KILLAGREG
#define PID_MM3_X_OFF		11 // byte
#define PID_MM3_Y_OFF		12 // byte
#define PID_MM3_Z_OFF		13 // byte
#define PID_MM3_X_RANGE		14 // word
#define PID_MM3_Y_RANGE		16 // word
#define PID_MM3_Z_RANGE		18 // word
#endif


#define EEPROM_ADR_CHANNELS	80		// 8 bytes

#define EEPROM_ADR_PARAMSET_LENGTH	98		// word
#define EEPROM_ADR_PARAMSET_BEGIN	100


#define EEPROM_ADR_MIXER_TABLE		1000 // 1000 - 1076


//! Bit mask for \c ParamSet.GlobalConfig.
#define CFG_HEIGHT_CONTROL			0x01
#define CFG_HEIGHT_SWITCH			0x02
#define CFG_HEADING_HOLD			0x04
#define CFG_COMPASS_ACTIVE			0x08
#define CFG_COMPASS_FIX				0x10
#define CFG_GPS_ACTIVE				0x20
#define CFG_AXIS_COUPLING_ACTIVE	0x40
#define CFG_ROTARY_RATE_LIMITER		0x80

//! Bit mask for \c ParamSet.BitConfig.
#define CFG_LOOP_UP			0x01
#define CFG_LOOP_DOWN		0x02
#define CFG_LOOP_LEFT		0x04
#define CFG_LOOP_RIGHT		0x08
#define CFG_HEIGHT_3SWITCH	0x10

// Defines for looking up paramset_t.ChannelAssignment.
#define CH_NICK   0 //!< Channel for nick.  \sa paramset_t.ChannelAssignment
#define CH_ROLL   1 //!< Channel for roll.  \sa paramset_t.ChannelAssignment
#define CH_GAS    2 //!< Channel for gas.   \sa paramset_t.ChannelAssignment
#define CH_YAW    3 //!< Channel for yaw.   \sa paramset_t.ChannelAssignment
#define CH_POTI1  4 //!< Channel for poti1. \sa paramset_t.ChannelAssignment
#define CH_POTI2  5 //!< Channel for poti2. \sa paramset_t.ChannelAssignment
#define CH_POTI3  6 //!< Channel for poti3. \sa paramset_t.ChannelAssignment
#define CH_POTI4  7 //!< Channel for poti4. \sa paramset_t.ChannelAssignment

/*! The current EEPROM data (layout) revision.
	\warning For data compatibility reasons this should be incremented, when paramset_t gets changed.
*/
#define EEPARAM_REVISION	75

/*! The current Mixer data (layout) revision.
	\warning For data compatibility reasons this should be incremented, when MixerTable_t gets changed.
*/
#define EEMIXER_REVISION	 1

/*! Struct for storing parameter set data.
	 \warning For data compatibility reasons #EEPARAM_REVISION should be incremented, when this gets changed.
*/
// values above 250 representing poti1 to poti4
typedef struct
{
	uint8_t ChannelAssignment[8];   //!< Holds the channel assignment as defined in the CH_* macros (e.g. #CH_GAS).
	uint8_t GlobalConfig;           // see upper defines for bitcoding
	uint8_t HeightMinGas;          // Wert : 0-100
	uint8_t HeightD;               // Wert : 0-250
	uint8_t MaxHeight;              // Wert : 0-32
	uint8_t HeightP;               // Wert : 0-32
	uint8_t Height_Gain;            // Wert : 0-50
	uint8_t Height_ACC_Effect;      // Wert : 0-250
	uint8_t StickP;                // Wert : 1-6
	uint8_t StickD;                // Wert : 0-64
	uint8_t StickYawP;                  // Wert : 1-20
	uint8_t GasMin;                // Wert : 0-32
	uint8_t GasMax;                // Wert : 33-250
	uint8_t GyroAccFactor;          // Wert : 1-64
	uint8_t CompassYawEffect;       // Wert : 0-32
	uint8_t GyroP;                 // Wert : 10-250
	uint8_t GyroI;                 // Wert : 0-250
	uint8_t GyroD;				   // Wert : 0-250
	uint8_t LowVoltageWarning;      // Wert : 0-250
	uint8_t EmergencyGas;           // Wert : 0-250     //Gaswert bei Empängsverlust
	uint8_t EmergencyGasDuration;   // Wert : 0-250     // Zeitbis auf EmergencyGas geschaltet wird, wg. Rx-Problemen
	uint8_t UfoArrangement;         // x oder + Formation
	uint8_t IFactor;               // Wert : 0-250
	uint8_t UserParam1;             // Wert : 0-250
	uint8_t UserParam2;             // Wert : 0-250
	uint8_t UserParam3;             // Wert : 0-250
	uint8_t UserParam4;             // Wert : 0-250
	uint8_t ServoNickControl;       // Wert : 0-250     // Stellung des Servos
	uint8_t ServoNickComp;          // Wert : 0-250     // Einfluss Gyro/Servo
	uint8_t ServoNickMin;           // Wert : 0-250     // Anschlag
	uint8_t ServoNickMax;           // Wert : 0-250     // Anschlag
	uint8_t ServoRefresh;       	// Wert: 0-250		// Refreshrate of servo pwm output
	uint8_t LoopGasLimit;           // Wert: 0-250  max. Gas während Looping
	uint8_t LoopThreshold;          // Wert: 0-250  Schwelle für Stickausschlag
	uint8_t LoopHysteresis;         // Wert: 0-250  Hysterese für Stickausschlag
	uint8_t AxisCoupling1;		   // Wert: 0-250  Faktor, mit dem Yaw die Achsen Roll und Nick koppelt (NickRollMitkopplung)
	uint8_t AxisCoupling2;		   // Wert: 0-250  Faktor, mit dem Nick und Roll verkoppelt werden
	uint8_t AxisCouplingYawCorrection;// Wert: 0-250  Faktor, mit dem Nick und Roll verkoppelt werden
	uint8_t AngleTurnOverNick;      // Wert: 0-250  180°-Punkt
	uint8_t AngleTurnOverRoll;      // Wert: 0-250  180°-Punkt
	uint8_t GyroAccTrim;            // 1/k  (Koppel_ACC_Wirkung)
	uint8_t DriftComp;              // limit for gyrodrift compensation
	uint8_t DynamicStability;       // PID limit for Attitude controller
	uint8_t UserParam5;             // Wert : 0-250
	uint8_t UserParam6;             // Wert : 0-250
	uint8_t UserParam7;             // Wert : 0-250
	uint8_t UserParam8;             // Wert : 0-250
	uint8_t J16Bitmask;			   // for the J16 Output
	uint8_t J16Timing;			   // for the J16 Output
	uint8_t J17Bitmask;			   // for the J17 Output
	uint8_t J17Timing;			   // for the J17 Output
	uint8_t NaviGpsModeControl;     // Parameters for the Naviboard
	uint8_t NaviGpsGain;		  // overall gain for GPS-PID controller
	uint8_t NaviGpsP;			   // P gain for GPS-PID controller
	uint8_t NaviGpsI;			   // I gain for GPS-PID controller
	uint8_t NaviGpsD;               // D gain for GPS-PID controller
	uint8_t NaviGpsPLimit;			// P limit for GPS-PID controller
	uint8_t NaviGpsILimit;			// I limit for GPS-PID controller
	uint8_t NaviGpsDLimit;          // D limit for GPS-PID controller
	uint8_t NaviGpsACC;             // ACC gain for GPS-PID controller
	uint8_t NaviGpsMinSat;          // number of sattelites neccesary for GPS functions
	uint8_t NaviStickThreshold;     // activation threshild for detection of manual stick movements
	uint8_t NaviWindCorrection;     // streng of wind course correction
	uint8_t NaviSpeedCompensation;  // D gain fefore position hold login
	uint8_t NaviOperatingRadius;	// Radius limit in m around start position for GPS flights
	uint8_t NaviAngleLimitation;	// limitation of attitude angle controlled by the gps algorithm
	uint8_t NaviPHLoginTime;		// position hold logintimeout
	uint8_t ExternalControl;        // for serial Control
	uint8_t BitConfig;              // see upper defines for bitcoding
	uint8_t ServoNickCompInvert;    // Wert : 0-250   0 oder 1  // WICHTIG!!! am Ende lassen
	uint8_t Reserved[4];
	int8_t Name[12];
 } paramset_t;

//! The length of a parameter set
#define  PARAMSET_STRUCT_LEN  sizeof(paramset_t)

//! The current set of parameters
extern paramset_t ParamSet;

extern void ParamSet_Init(void);
extern void ParamSet_ReadFromEEProm(uint8_t setnumber);
extern void ParamSet_WriteToEEProm(uint8_t setnumber);
extern uint8_t GetActiveParamSet(void);
extern void SetActiveParamSet(uint8_t setnumber);

extern uint8_t MixerTable_ReadFromEEProm(void);
extern uint8_t MixerTable_WriteToEEProm(void);


extern uint8_t GetParamByte(uint16_t param_id);
extern void SetParamByte(uint16_t param_id, uint8_t value);
extern uint16_t GetParamWord(uint16_t param_id);
extern void SetParamWord(uint16_t param_id, uint16_t value);


#endif //_EEPROM_H
