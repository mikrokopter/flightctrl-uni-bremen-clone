/*#######################################################################################
Flight Control
#######################################################################################*/
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Copyright (c) 04.2007 Holger Buss
// + Nur für den privaten Gebrauch
// + www.MikroKopter.com
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Es gilt für das gesamte Projekt (Hardware, Software, Binärfiles, Sourcecode und Dokumentation),
// + dass eine Nutzung (auch auszugsweise) nur für den privaten (nicht-kommerziellen) Gebrauch zulässig ist.
// + Sollten direkte oder indirekte kommerzielle Absichten verfolgt werden, ist mit uns (info@mikrokopter.de) Kontakt
// + bzgl. der Nutzungsbedingungen aufzunehmen.
// + Eine kommerzielle Nutzung ist z.B.Verkauf von MikroKoptern, Bestückung und Verkauf von Platinen oder Bausätzen,
// + Verkauf von Luftbildaufnahmen, usw.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Werden Teile des Quellcodes (mit oder ohne Modifikation) weiterverwendet oder veröffentlicht,
// + unterliegen sie auch diesen Nutzungsbedingungen und diese Nutzungsbedingungen incl. Copyright müssen dann beiliegen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Sollte die Software (auch auszugesweise) oder sonstige Informationen des MikroKopter-Projekts
// + auf anderen Webseiten oder sonstigen Medien veröffentlicht werden, muss unsere Webseite "http://www.mikrokopter.de"
// + eindeutig als Ursprung verlinkt werden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Keine Gewähr auf Fehlerfreiheit, Vollständigkeit oder Funktion
// + Benutzung auf eigene Gefahr
// + Wir übernehmen keinerlei Haftung für direkte oder indirekte Personen- oder Sachschäden
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Portierung der Software (oder Teile davon) auf andere Systeme (ausser der Hardware von www.mikrokopter.de) ist nur
// + mit unserer Zustimmung zulässig
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Die Funktion printf_P() unterliegt ihrer eigenen Lizenz und ist hiervon nicht betroffen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Redistributions of source code (with or without modifications) must retain the above copyright notice,
// + this list of conditions and the following disclaimer.
// +   * Neither the name of the copyright holders nor the names of contributors may be used to endorse or promote products derived
// +     from this software without specific prior written permission.
// +   * The use of this project (hardware, software, binary files, sources and documentation) is only permittet
// +     for non-commercial use (directly or indirectly)
// +     Commercial use (for excample: selling of MikroKopters, selling of PCBs, assembly, ...) is only permitted
// +     with our written permission
// +   * If sources or documentations are redistributet on other webpages, out webpage (http://www.MikroKopter.de) must be
// +     clearly linked as origin
// +   * porting to systems other than hardware from www.mikrokopter.de is not allowed
// +  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// +  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// +  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// +  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// +  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// +  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// +  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// +  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN// +  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// +  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// +  POSSIBILITY OF SUCH DAMAGE.
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include <stdlib.h>
#include <avr/io.h>

#include "main.h"
#include "eeprom.h"
#include "timer0.h"
#include "analog.h"
#include "fc.h"
#include "motor.h"
#include "uart0.h"
#include "rc.h"
#include "twimaster.h"
#include "timer2.h"
#ifdef USE_KILLAGREG
	#include "mm3.h"
	#include "gps.h"
#endif
#ifdef USE_MK3MAG
	#include "mk3mag.h"
	#include "gps.h"
#endif
#include "led.h"
#ifdef USE_NAVICTRL
	#include "spi.h"
#endif


int16_t GyroPitch;
int16_t GyroRoll;
int16_t GyroYaw;

int16_t BiasHiResGyroPitch = 0;
int16_t BiasHiResGyroRoll = 0;
int16_t AdBiasGyroYaw = 0;

int16_t AccPitch;
int16_t AccRoll;
int16_t AccTop;

int16_t AdBiasAccPitch = 0;
int16_t AdBiasAccRoll = 0;
volatile float AdBiasAccTop = 0;

int16_t TrimPitch;
int16_t TrimRoll;

int32_t IntegralGyroPitch = 0;
int32_t IntegralGyroPitch2 = 0;
int32_t IntegralGyroRoll = 0;
int32_t IntegralGyroRoll2 = 0;
int32_t IntegralGyroYaw = 0;
int32_t ReadingIntegralGyroPitch = 0;
int32_t ReadingIntegralGyroPitch2 = 0;
int32_t ReadingIntegralGyroRoll = 0;
int32_t ReadingIntegralGyroRoll2 = 0;
int32_t ReadingIntegralGyroYaw = 0;
int32_t MeanIntegralGyroPitch;
int32_t MeanIntegralGyroRoll;

int32_t MeanAccPitch = 0;
int32_t MeanAccRoll = 0;
volatile int32_t ReadingIntegralTop = 0;

int16_t CompassHeading = -1;
int16_t CompassCourse = -1;
int16_t CompassOffCourse = 0;
uint8_t CompassCalState = 0;
uint8_t FunnelCourse = 0;
uint16_t BadCompassHeading = 500;
int32_t YawGyroHeading;
int16_t YawGyroDrift;

int16_t NaviAccPitch = 0;
int16_t NaviAccRoll = 0;
int16_t NaviCntAcc = 0;

uint16_t ModelIsFlying = 0;
uint8_t volatile MKFlags = 0;

int32_t TurnOver180Pitch = 250000L;
int32_t TurnOver180Roll = 250000L;

uint8_t GyroPFactor;
uint8_t GyroIFactor;
uint8_t GyroYawPFactor;
uint8_t GyroYawIFactor;

int16_t Ki = 10300 / 33;

int16_t Poti1 = 0;
int16_t Poti2 = 0;
int16_t Poti3 = 0;
int16_t Poti4 = 0;
int16_t Poti5 = 0;
int16_t Poti6 = 0;
int16_t Poti7 = 0;
int16_t Poti8 = 0;

int16_t StickPitch = 0;
int16_t StickRoll = 0;
int16_t StickYaw = 0;
int16_t StickGas = 0;
int16_t GPSStickPitch = 0;
int16_t GPSStickRoll = 0;

int16_t MaxStickPitch = 0;
int16_t MaxStickRoll = 0;

int16_t ExternHeightValue = -20;

int16_t ReadingHeight = 0;
int16_t SetPointHeight = 0;

int16_t AttitudeCorrectionRoll = 0;
int16_t AttitudeCorrectionPitch = 0;

bool LoopingPitch = false;
bool LoopingRoll = false;
bool LoopingLeft = false;
bool LoopingRight = false;
bool LoopingDown = false;
bool LoopingTop = false;

fc_param_t FCParam = {48,251,16,58,64,8,150,150,2,10,0,0,0,0,0,0,0,0,100,70,90,65,64,100,0,0,0};

void SetNeutral(bool DoAccAdjustment)
{
	uint8_t i;
	int32_t Sum_1;
	int32_t Sum_2;
	int32_t Sum_3;

	Servo_Off(); // disable servo output

	AdBiasAccPitch = 0;
	AdBiasAccRoll = 0;
	AdBiasAccTop = 0;

	BiasHiResGyroPitch = 0;
	BiasHiResGyroRoll = 0;
	AdBiasGyroYaw = 0;

	FCParam.AxisCoupling1 = 0;
	FCParam.AxisCoupling2 = 0;

	ExpandBaro = 0;

	// sample values with bias set to zero
	Delay_ms_Mess(100);

	if(BoardRelease == 13) {
		SearchDacGyroOffset();
	}

	if(ParamSet.GlobalConfig & CFG_HEIGHT_CONTROL) // height control activated?
	{
		if((ReadingAirPressure > 950) || (ReadingAirPressure < 750)) {
			SearchAirPressureOffset();
		}
	}

	// determine gyro bias by averaging (require no rotation movement)
	#define GYRO_BIAS_AVERAGE 32
	Sum_1 = 0;
	Sum_2 = 0;
	Sum_3 = 0;
	for(i=0; i < GYRO_BIAS_AVERAGE; i++)
	{
		Delay_ms_Mess(10);
		Sum_1 += AdValueGyroNick * HIRES_GYRO_AMPLIFY;
		Sum_2 += AdValueGyroRoll * HIRES_GYRO_AMPLIFY;
		Sum_3 += AdValueGyroYaw;
	}
	BiasHiResGyroPitch = (int16_t)((Sum_1 + GYRO_BIAS_AVERAGE / 2) / GYRO_BIAS_AVERAGE);
	BiasHiResGyroRoll  = (int16_t)((Sum_2 + GYRO_BIAS_AVERAGE / 2) / GYRO_BIAS_AVERAGE);
	AdBiasGyroYaw      = (int16_t)((Sum_3 + GYRO_BIAS_AVERAGE / 2) / GYRO_BIAS_AVERAGE);

	if(DoAccAdjustment) {
		SetNeutral_SaveAccBias();
	} else { // restore from eeprom
		SetNeutral_RestoreAccBias();
	}
	// setting acc bias values has an influence in the analog.c ISR
	// therefore run measurement for 100ms to achive stable readings
	Delay_ms_Mess(100);

	// reset acc averaging and integrals
	AccPitch = ACC_AMPLIFY * (int32_t)AdValueAccNick;
	AccRoll  = ACC_AMPLIFY * (int32_t)AdValueAccRoll;
	AccTop   = AdValueAccTop;
	ReadingIntegralTop = AdValueAccTop;

	// and gyro readings
	GyroPitch = 0;
	GyroRoll  = 0;
	GyroYaw   = 0;

	// reset gyro integrals to acc guessing
	IntegralGyroPitch = ParamSet.GyroAccFactor * (int32_t)AccPitch;
	IntegralGyroRoll  = ParamSet.GyroAccFactor * (int32_t)AccRoll;
	//ReadingIntegralGyroPitch = IntegralGyroPitch;
	//ReadingIntegralGyroRoll = IntegralGyroRoll;
	ReadingIntegralGyroPitch2 = IntegralGyroPitch;
	ReadingIntegralGyroRoll2  = IntegralGyroRoll;
	ReadingIntegralGyroYaw    = 0;


	StartAirPressure = AirPressure;
	HeightD = 0;

	// update compass course to current heading
	CompassCourse = CompassHeading;
	// Inititialize YawGyroIntegral value with current compass heading
	YawGyroHeading = (int32_t)CompassHeading * GYRO_DEG_FACTOR;
	YawGyroDrift   = 0;

	Beep_SetBeepTime(5);

	TurnOver180Pitch = ((int32_t) ParamSet.AngleTurnOverNick * 2500L) +15000L;
	TurnOver180Roll  = ((int32_t) ParamSet.AngleTurnOverRoll * 2500L) +15000L;

	ExternHeightValue = 0;

	GPSStickPitch = 0;
	GPSStickRoll = 0;

	MKFlags |= MKFLAG_CALIBRATE;

	FCParam.KalmanK = -1;
	FCParam.KalmanMaxDrift = 0;
	FCParam.KalmanMaxFusion = 32;

	Poti1 = PPM_in[ParamSet.ChannelAssignment[CH_POTI1]] + 110;
	Poti2 = PPM_in[ParamSet.ChannelAssignment[CH_POTI2]] + 110;
	Poti3 = PPM_in[ParamSet.ChannelAssignment[CH_POTI3]] + 110;
	Poti4 = PPM_in[ParamSet.ChannelAssignment[CH_POTI4]] + 110;

	Servo_On(); //enable servo output
	RC_Quality = 100;
}

inline void SetNeutral_SaveAccBias(void)
{
	// determine acc bias by averaging (require horizontal adjustment in nick and roll attitude)
	int32_t Sum_1 = 0;
	int32_t Sum_2 = 0;
	int32_t Sum_3 = 0;

	#define ACC_BIAS_AVERAGE 10
	for(uint8_t i=0; i < ACC_BIAS_AVERAGE; i++) {
		Delay_ms_Mess(10);
		Sum_1 += AdValueAccNick;
		Sum_2 += AdValueAccRoll;
		Sum_3 += AdValueAccZ;
	}
	// use abs() to avoid negative bias settings because of adc sign flip in adc.c
	AdBiasAccPitch = (int16_t)((abs(Sum_1) + ACC_BIAS_AVERAGE / 2) / ACC_BIAS_AVERAGE);
	AdBiasAccRoll  = (int16_t)((abs(Sum_2) + ACC_BIAS_AVERAGE / 2) / ACC_BIAS_AVERAGE);
	AdBiasAccTop   = (int16_t)((abs(Sum_3) + ACC_BIAS_AVERAGE / 2) / ACC_BIAS_AVERAGE);

	// Save ACC neutral settings to eeprom
	SetParamWord(PID_ACC_NICK, (uint16_t)AdBiasAccPitch);
	SetParamWord(PID_ACC_ROLL, (uint16_t)AdBiasAccRoll);
	SetParamWord(PID_ACC_TOP,  (uint16_t)AdBiasAccTop);
}

inline void SetNeutral_RestoreAccBias(void)
{
	AdBiasAccPitch = (int16_t)GetParamWord(PID_ACC_NICK);
	AdBiasAccRoll  = (int16_t)GetParamWord(PID_ACC_ROLL);
	AdBiasAccTop   = (int16_t)GetParamWord(PID_ACC_TOP);
}

void Mean(void)
{
	int32_t tmpl  = 0;
	int32_t tmpl2 = 0;
	int32_t tmp13 = 0;
	int32_t tmp14 = 0;
	int16_t FilterGyroNick;
	int16_t FilterGyroRoll;
	static int16_t Last_GyroRoll = 0;
	static int16_t Last_GyroNick = 0;
	int16_t d2Nick;
	int16_t d2Roll;
	int32_t AngleNick;
	int32_t AngleRoll;
	int16_t CouplingNickRoll = 0;
	int16_t CouplingRollNick = 0;

	// Get bias free gyro readings
	GyroPitch = HiResGyroNick / HIRES_GYRO_AMPLIFY; // unfiltered gyro rate
	FilterGyroNick = FilterHiResGyroNick / HIRES_GYRO_AMPLIFY; // use filtered gyro rate

	// handle rotation rates that violate adc ranges
	if(AdValueGyroNick < 15)   GyroPitch = -1000;
	if(AdValueGyroNick <  7)   GyroPitch = -2000;
	if(BoardRelease == 10)
	{
		if(AdValueGyroNick > 1010) GyroPitch = +1000;
		if(AdValueGyroNick > 1017) GyroPitch = +2000;
	}
	else
	{
		if(AdValueGyroNick > 2000) GyroPitch = +1000;
		if(AdValueGyroNick > 2015) GyroPitch = +2000;
	}

	GyroRoll = HiResGyroRoll / HIRES_GYRO_AMPLIFY; // unfiltered gyro rate
	FilterGyroRoll = FilterHiResGyroRoll / HIRES_GYRO_AMPLIFY; // use filtered gyro rate
	// handle rotation rates that violate adc ranges
	if(AdValueGyroRoll < 15)   GyroRoll = -1000;
	if(AdValueGyroRoll <  7)   GyroRoll = -2000;
	if(BoardRelease == 10)
	{
		if(AdValueGyroRoll > 1010) GyroRoll = +1000;
		if(AdValueGyroRoll > 1017) GyroRoll = +2000;
	}
	else
	{
		if(AdValueGyroRoll > 2000) GyroRoll = +1000;
		if(AdValueGyroRoll > 2015) GyroRoll = +2000;
	}

	GyroYaw = AdBiasGyroYaw - AdValueGyroYaw;

	// Acceleration Sensor
	// lowpass acc measurement and scale AccPitch/AccRoll by a factor of ACC_AMPLIFY to have a better resolution
	AccPitch = ((int32_t)AccPitch * 3 + ((ACC_AMPLIFY * (int32_t)AdValueAccNick))) / 4L;
	AccRoll  = ((int32_t)AccRoll * 3 + ((ACC_AMPLIFY * (int32_t)AdValueAccRoll))) / 4L;
	AccTop   = ((int32_t)AccTop  * 3 + ((int32_t)AdValueAccTop)) / 4L;

	// sum acc sensor readings for later averaging
	MeanAccPitch  += ACC_AMPLIFY * AdValueAccNick;
	MeanAccRoll  += ACC_AMPLIFY * AdValueAccRoll;

	NaviAccPitch += AdValueAccNick;
	NaviAccRoll += AdValueAccRoll;
	NaviCntAcc++;

	// enable ADC to meassure next readings, before that point all variables should be read that are written by the ADC ISR
	ADC_Enable();
	ADReady = 0;

	// limit angle readings for axis coupling calculations
	#define ANGLE_LIMIT 93000L // aprox. 93000/GYRO_DEG_FACTOR = 82 deg

	AngleNick = ReadingIntegralGyroPitch;
	EnsureValueInBounds(AngleNick, -ANGLE_LIMIT, ANGLE_LIMIT);

	AngleRoll = ReadingIntegralGyroRoll;
	EnsureValueInBounds(AngleRoll, -ANGLE_LIMIT, ANGLE_LIMIT);


	// Yaw
	// calculate yaw gyro integral (~ to rotation angle)
	YawGyroHeading += GyroYaw;
	ReadingIntegralGyroYaw  += GyroYaw;


	// Coupling fraction
	if(! LoopingPitch && !LoopingRoll && (ParamSet.GlobalConfig & CFG_AXIS_COUPLING_ACTIVE))
	{
		tmp13 = (FilterGyroRoll * AngleNick) / 2048L;
		tmp13 *= FCParam.AxisCoupling2; // 65
		tmp13 /= 4096L;
		CouplingNickRoll = tmp13;

		tmp14 = (FilterGyroNick * AngleRoll) / 2048L;
		tmp14 *= FCParam.AxisCoupling2; // 65
		tmp14 /= 4096L;
		CouplingRollNick = tmp14;

		tmp14 -= tmp13;
		YawGyroHeading += tmp14;
		if(!FCParam.AxisCouplingYawCorrection)  ReadingIntegralGyroYaw -= tmp14 / 2; // force yaw

		tmpl = ((GyroYaw + tmp14) * AngleNick) / 2048L;
		tmpl *= FCParam.AxisCoupling1;
		tmpl /= 4096L;

		tmpl2 = ((GyroYaw + tmp14) * AngleRoll) / 2048L;
		tmpl2 *= FCParam.AxisCoupling1;
		tmpl2 /= 4096L;
		if(abs(GyroYaw > 64))
		{
			if(labs(tmpl) > 128 || labs(tmpl2) > 128) FunnelCourse = 1;
		}

		TrimPitch = -tmpl2 + tmpl / 100L;
		TrimRoll = tmpl - tmpl2 / 100L;
	}
	else
	{
		CouplingNickRoll = 0;
		CouplingRollNick = 0;
		TrimPitch = 0;
		TrimRoll = 0;
	}


	// Yaw

	// limit YawGyroHeading proportional to 0° to 360°
	if(YawGyroHeading >= (360L * GYRO_DEG_FACTOR)) YawGyroHeading -= 360L * GYRO_DEG_FACTOR;  // 360° Wrap
	if(YawGyroHeading < 0)                         YawGyroHeading += 360L * GYRO_DEG_FACTOR;

	// Roll
	ReadingIntegralGyroRoll2 += FilterGyroRoll + TrimRoll;
	ReadingIntegralGyroRoll  += FilterGyroRoll + TrimRoll- AttitudeCorrectionRoll;
	if(ReadingIntegralGyroRoll > TurnOver180Roll)
	{
		ReadingIntegralGyroRoll  = -(TurnOver180Roll - 10000L);
		ReadingIntegralGyroRoll2 = ReadingIntegralGyroRoll;
	}
	if(ReadingIntegralGyroRoll < -TurnOver180Roll)
	{
		ReadingIntegralGyroRoll =  (TurnOver180Roll - 10000L);
		ReadingIntegralGyroRoll2 = ReadingIntegralGyroRoll;
	}

	// Nick
	ReadingIntegralGyroPitch2 += FilterGyroNick + TrimPitch;
	ReadingIntegralGyroPitch  += FilterGyroNick + TrimPitch - AttitudeCorrectionPitch;
	if(ReadingIntegralGyroPitch > TurnOver180Pitch)
	{
		ReadingIntegralGyroPitch = -(TurnOver180Pitch - 25000L);
		ReadingIntegralGyroPitch2 = ReadingIntegralGyroPitch;
	}
	if(ReadingIntegralGyroPitch < -TurnOver180Pitch)
	{
		ReadingIntegralGyroPitch =  (TurnOver180Pitch - 25000L);
		ReadingIntegralGyroPitch2 = ReadingIntegralGyroPitch;
	}

	IntegralGyroYaw   = ReadingIntegralGyroYaw;
	IntegralGyroPitch = ReadingIntegralGyroPitch;
	IntegralGyroRoll  = ReadingIntegralGyroRoll;
	IntegralGyroPitch2 = ReadingIntegralGyroPitch2;
	IntegralGyroRoll2 = ReadingIntegralGyroRoll2;


	#define D_LIMIT 128

	if(FCParam.GyroD)
	{
		d2Nick = (HiResGyroNick - Last_GyroNick); // change of gyro rate
		Last_GyroNick = (Last_GyroNick + HiResGyroNick) / 2;
		EnsureValueInBounds(d2Nick, -D_LIMIT, D_LIMIT);
		GyroPitch += (d2Nick * (int16_t)FCParam.GyroD) / 16;

		d2Roll = (HiResGyroRoll - Last_GyroRoll); // change of gyro rate
		Last_GyroRoll = (Last_GyroRoll + HiResGyroRoll) / 2;
		EnsureValueInBounds(d2Roll, -D_LIMIT, D_LIMIT);
		GyroRoll += (d2Roll * (int16_t)FCParam.GyroD) / 16;

		HiResGyroNick += (d2Nick * (int16_t)FCParam.GyroD);
		HiResGyroRoll += (d2Roll * (int16_t)FCParam.GyroD);
	}

	// Increase the roll/nick rate virtually proportional to the coupling to suppress a faster rotation
	if(FilterGyroNick > 0) TrimPitch += ((int32_t)abs(CouplingRollNick) * FCParam.AxisCouplingYawCorrection) / 64L;
	else                   TrimPitch -= ((int32_t)abs(CouplingRollNick) * FCParam.AxisCouplingYawCorrection) / 64L;
	if(FilterGyroRoll > 0) TrimRoll += ((int32_t)abs(CouplingNickRoll) * FCParam.AxisCouplingYawCorrection) / 64L;
	else                   TrimRoll -= ((int32_t)abs(CouplingNickRoll) * FCParam.AxisCouplingYawCorrection) / 64L;

	// increase the nick/roll rates virtually from the threshold of 245 to slow down higher rotation rates
	if((ParamSet.GlobalConfig & CFG_ROTARY_RATE_LIMITER) && ! LoopingPitch && !LoopingRoll)
	{
		if(FilterGyroNick > 256) GyroPitch += 1 * (FilterGyroNick - 256);
		else if(FilterGyroNick < -256) GyroPitch += 1 * (FilterGyroNick + 256);
		if(FilterGyroRoll > 256)       GyroRoll += 1 * (FilterGyroRoll - 256);
		else if(FilterGyroRoll < -256) GyroRoll += 1 * (FilterGyroRoll + 256);
	}

}

void SendMotorData(void)
{
	if(!(MKFlags & MKFLAG_MOTOR_RUN)) { // motors are not running
		MKFlags &= ~(MKFLAG_FLY|MKFLAG_START); // clear flag FLY and START if motors are off

		for(uint8_t i = 0; i < Motor_Count; i++) {
			if(!MotorTest_Active) {
				Motor[i].Setpoint = 0;
			} else {
				Motor[i].Setpoint = MotorTest[i];
			}
		}

		if(MotorTest_Active) {
			MotorTest_Active--;
		}
	}

	DebugOut.Analog[12] = Motor[Motor_Index_Front].Setpoint;
	DebugOut.Analog[13] = Motor[Motor_Index_Rear].Setpoint;
	DebugOut.Analog[14] = Motor[Motor_Index_Left].Setpoint;
	DebugOut.Analog[15] = Motor[Motor_Index_Right].Setpoint;
	//Start I2C Interrupt Mode
	TWI_Start(TWI_STATE_MOTOR_TX);
}

void ParameterMapping(void)
{
	if(RC_Quality > 160) // do the mapping of RC-Potis only if the rc-signal is ok
	// else the last updated values are used
	{
		//update poti values by rc-signals
		#define CHK_POTI_MM(b,a,min,max) { if(a > 250) { if(a == 251) b = Poti1; else if(a == 252) b = Poti2; else if(a == 253) b = Poti3; else if(a == 254) b = Poti4;} else b = a; if(b <= min) b = min; else if(b >= max) b = max;}
		#define CHK_POTI(b,a) { if(a > 250) { if(a == 251) b = Poti1; else if(a == 252) b = Poti2; else if(a == 253) b = Poti3; else if(a == 254) b = Poti4;} else b = a;}
		CHK_POTI(FCParam.MaxHeight,ParamSet.MaxHeight);
		CHK_POTI_MM(FCParam.HeightD,ParamSet.HeightD,0,100);
		CHK_POTI_MM(FCParam.HeightP,ParamSet.HeightP,0,100);
		CHK_POTI(FCParam.Height_ACC_Effect,ParamSet.Height_ACC_Effect);
		CHK_POTI(FCParam.CompassYawEffect,ParamSet.CompassYawEffect);
		CHK_POTI_MM(FCParam.GyroP,ParamSet.GyroP,10,255);
		CHK_POTI(FCParam.GyroI,ParamSet.GyroI);
		CHK_POTI(FCParam.GyroD,ParamSet.GyroD);
		CHK_POTI(FCParam.IFactor,ParamSet.IFactor);
		CHK_POTI(FCParam.UserParam1,ParamSet.UserParam1);
		CHK_POTI(FCParam.UserParam2,ParamSet.UserParam2);
		CHK_POTI(FCParam.UserParam3,ParamSet.UserParam3);
		CHK_POTI(FCParam.UserParam4,ParamSet.UserParam4);
		CHK_POTI(FCParam.UserParam5,ParamSet.UserParam5);
		CHK_POTI(FCParam.UserParam6,ParamSet.UserParam6);
		CHK_POTI(FCParam.UserParam7,ParamSet.UserParam7);
		CHK_POTI(FCParam.UserParam8,ParamSet.UserParam8);
		CHK_POTI(FCParam.ServoNickControl,ParamSet.ServoNickControl);
		CHK_POTI(FCParam.LoopGasLimit,ParamSet.LoopGasLimit);
		CHK_POTI(FCParam.AxisCoupling1,ParamSet.AxisCoupling1);
		CHK_POTI(FCParam.AxisCoupling2,ParamSet.AxisCoupling2);
		CHK_POTI(FCParam.AxisCouplingYawCorrection,ParamSet.AxisCouplingYawCorrection);
		CHK_POTI(FCParam.DynamicStability,ParamSet.DynamicStability);
		CHK_POTI_MM(FCParam.J16Timing,ParamSet.J16Timing,1,255);
		CHK_POTI_MM(FCParam.J17Timing,ParamSet.J17Timing,1,255);
#if (defined (USE_KILLAGREG) || defined (USE_MK3MAG))
		CHK_POTI(FCParam.NaviGpsModeControl,ParamSet.NaviGpsModeControl);
		CHK_POTI(FCParam.NaviGpsGain,ParamSet.NaviGpsGain);
		CHK_POTI(FCParam.NaviGpsP,ParamSet.NaviGpsP);
		CHK_POTI(FCParam.NaviGpsI,ParamSet.NaviGpsI);
		CHK_POTI(FCParam.NaviGpsD,ParamSet.NaviGpsD);
		CHK_POTI(FCParam.NaviGpsACC,ParamSet.NaviGpsACC);
		CHK_POTI_MM(FCParam.NaviOperatingRadius,ParamSet.NaviOperatingRadius,10, 255);
		CHK_POTI(FCParam.NaviWindCorrection,ParamSet.NaviWindCorrection);
		CHK_POTI(FCParam.NaviSpeedCompensation,ParamSet.NaviSpeedCompensation);
#endif
		CHK_POTI(FCParam.ExternalControl,ParamSet.ExternalControl);
		Ki = 10300 / ( FCParam.IFactor + 1 );
	}
}

void SetCompassCalState(void)
{
	static uint8_t stick = 1;

	// if nick is centered or top set stick to zero
	if(PPM_in[ParamSet.ChannelAssignment[CH_NICK]] > -20) {
		stick = 0;
	}

	// if nick is down trigger to next cal state
	if((PPM_in[ParamSet.ChannelAssignment[CH_NICK]] < -70) && !stick) {
		stick = 1;
		CompassCalState++;
		if(CompassCalState < 5) {
			Beep_Beep(CompassCalState);
		} else {
			Beep_SetBeepTime(100);
		}
	}
}

void MotorControl(void)
{
	int16_t h;
	int16_t tmp_int;

	// Mixer Fractions that are combined for Motor Control
	int16_t YawMixFraction, GasMixFraction, PitchMixFraction, RollMixFraction;

	// PID controller variables
	int16_t DiffNick;
	int16_t DiffRoll;
	int16_t PDPartNick;
	int16_t PDPartRoll;
	int16_t PDPartYaw;
	int16_t PPartNick;
	int16_t PPartRoll;
	static int32_t IPartNick = 0;
	static int32_t IPartRoll = 0;

	static int32_t SetPointYaw = 0;
	static int32_t IntegralGyroNickError = 0;
	static int32_t IntegralGyroRollError = 0;
	static int32_t CorrectionNick;
	static int32_t CorrectionRoll;
	static uint16_t RcLostTimer;
	static uint8_t delay_neutral = 0;
	static uint8_t delay_startmotors = 0;
	static uint8_t delay_stopmotors = 0;
	static uint8_t HeightControlActive = 0;
	static int16_t HeightControlGas = 0;
	static int8_t TimerDebugOut = 0;
	static uint16_t UpdateCompassCourse = 0;


	Mean();
	LED_Green_Set(true);

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// determine gas value
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	GasMixFraction = StickGas;
	if(GasMixFraction < ParamSet.GasMin + 10) {
		GasMixFraction = ParamSet.GasMin + 10;
	}
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// RC-signal is bad
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	if(RC_Quality < 120)  // the rc-frame signal is not reveived or noisy
	{
		if(!PcAccess) // if also no PC-Access via UART
		{
			if(BeepModulation == 0xFFFF) {
				Beep_SetBeepTime(1500);
				BeepModulation = 0x0C00;
			}
		}
		if(RcLostTimer) {  // decremtent timer after rc sigal lost
			RcLostTimer--;
		} else { // rc lost countdown finished
			MKFlags &= ~(MKFLAG_MOTOR_RUN|MKFLAG_EMERGENCY_LANDING); // clear motor run flag that stop the motors in SendMotorData()
		}
		LED_Red_Set(true); // set red led
		if(ModelIsFlying > 1000) { // wahrscheinlich in der Luft --> langsam absenken
			GasMixFraction = ParamSet.EmergencyGas; // set emergency gas
			MKFlags |= (MKFLAG_EMERGENCY_LANDING); // ser flag fpr emergency landing
			// set neutral rc inputs
			PPM_diff[ParamSet.ChannelAssignment[CH_NICK]] = 0;
			PPM_diff[ParamSet.ChannelAssignment[CH_ROLL]] = 0;
			PPM_diff[ParamSet.ChannelAssignment[CH_YAW]] = 0;
			PPM_in[ParamSet.ChannelAssignment[CH_NICK]] = 0;
			PPM_in[ParamSet.ChannelAssignment[CH_ROLL]] = 0;
			PPM_in[ParamSet.ChannelAssignment[CH_YAW]] = 0;
		} else {
			MKFlags &= ~(MKFLAG_MOTOR_RUN); // clear motor run flag that stop the motors in SendMotorData()
		}
	} // eof RC_Quality < 120
	else
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// RC-signal is good
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	if(RC_Quality > 140)
	{
		MKFlags &= ~(MKFLAG_EMERGENCY_LANDING); // clear flag for emergency landing
		// reset emergency timer
		RcLostTimer = ParamSet.EmergencyGasDuration * 50;
		if(GasMixFraction > 40 && (MKFlags & MKFLAG_MOTOR_RUN) )
		{
			if(ModelIsFlying < 0xFFFF) ModelIsFlying++;
		}
		if(ModelIsFlying < 256)
		{
			IPartNick = 0;
			IPartRoll = 0;
			StickYaw = 0;
			if(ModelIsFlying == 250)
			{
				UpdateCompassCourse = 1;
				ReadingIntegralGyroYaw = 0;
				SetPointYaw = 0;
			}
		}
		else MKFlags |= (MKFLAG_FLY); // set fly flag

		if(Poti1 < PPM_in[ParamSet.ChannelAssignment[CH_POTI1]] + 110) Poti1++; else if(Poti1 > PPM_in[ParamSet.ChannelAssignment[CH_POTI1]] + 110 && Poti1) Poti1--;
		if(Poti2 < PPM_in[ParamSet.ChannelAssignment[CH_POTI2]] + 110) Poti2++; else if(Poti2 > PPM_in[ParamSet.ChannelAssignment[CH_POTI2]] + 110 && Poti2) Poti2--;
		if(Poti3 < PPM_in[ParamSet.ChannelAssignment[CH_POTI3]] + 110) Poti3++; else if(Poti3 > PPM_in[ParamSet.ChannelAssignment[CH_POTI3]] + 110 && Poti3) Poti3--;
		if(Poti4 < PPM_in[ParamSet.ChannelAssignment[CH_POTI4]] + 110) Poti4++; else if(Poti4 > PPM_in[ParamSet.ChannelAssignment[CH_POTI4]] + 110 && Poti4) Poti4--;
		//PPM24-Extension
		if(Poti5 < PPM_in[9] + 110)  Poti5++; else if(Poti5 >  PPM_in[9] + 110 && Poti5) Poti5--;
		if(Poti6 < PPM_in[10] + 110) Poti6++; else if(Poti6 > PPM_in[10] + 110 && Poti6) Poti6--;
		if(Poti7 < PPM_in[11] + 110) Poti7++; else if(Poti7 > PPM_in[11] + 110 && Poti7) Poti7--;
		if(Poti8 < PPM_in[12] + 110) Poti8++; else if(Poti8 > PPM_in[12] + 110 && Poti8) Poti8--;
		//limit poti values
		if(Poti1 < 0) Poti1 = 0; else if(Poti1 > 255) Poti1 = 255;
		if(Poti2 < 0) Poti2 = 0; else if(Poti2 > 255) Poti2 = 255;
		if(Poti3 < 0) Poti3 = 0; else if(Poti3 > 255) Poti3 = 255;
		if(Poti4 < 0) Poti4 = 0; else if(Poti4 > 255) Poti4 = 255;
		//PPM24-Extension
		if(Poti5 < 0) Poti5 = 0; else if(Poti5 > 255) Poti5 = 255;
		if(Poti6 < 0) Poti6 = 0; else if(Poti6 > 255) Poti6 = 255;
		if(Poti7 < 0) Poti7 = 0; else if(Poti7 > 255) Poti7 = 255;
		if(Poti8 < 0) Poti8 = 0; else if(Poti8 > 255) Poti8 = 255;

		// if motors are off and the gas stick is in the upper position
		if((PPM_in[ParamSet.ChannelAssignment[CH_GAS]] > 80) && !(MKFlags & MKFLAG_MOTOR_RUN) )
		{
			// and if the yaw stick is in the leftmost position
			if(PPM_in[ParamSet.ChannelAssignment[CH_YAW]] > 75)
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// calibrate the neutral readings of all attitude sensors
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			{
				// gas/yaw joystick is top left
				//  _________
				// |x        |
				// |         |
				// |         |
				// |         |
				// |         |
				//  ¯¯¯¯¯¯¯¯¯
				if(++delay_neutral > 200)  // not immediately (wait 200 loops = 200 * 2ms = 0.4 s)
				{
					delay_neutral = 0;
					LED_Green_Set(false);
					ModelIsFlying = 0;
					// check roll/nick stick position
					// if nick stick is top or roll stick is left or right --> change parameter setting
					// according to roll/nick stick position
					if(PPM_in[ParamSet.ChannelAssignment[CH_NICK]] > 70 || abs(PPM_in[ParamSet.ChannelAssignment[CH_ROLL]]) > 70)
					{
						 uint8_t setting = 1; // default
						 // nick/roll joystick
						 //  _________
						 // |2   3   4|
						 // |         |
						 // |1       5|
						 // |         |
						 // |         |
						 //  ¯¯¯¯¯¯¯¯¯
						 // roll stick leftmost and nick stick centered --> setting 1
						 if(PPM_in[ParamSet.ChannelAssignment[CH_ROLL]] > 70 && PPM_in[ParamSet.ChannelAssignment[CH_NICK]] < 70) setting = 1;
						 // roll stick leftmost and nick stick topmost --> setting 2
						 if(PPM_in[ParamSet.ChannelAssignment[CH_ROLL]] > 70 && PPM_in[ParamSet.ChannelAssignment[CH_NICK]] > 70) setting = 2;
						 // roll stick centered an nick stick topmost --> setting 3
						 if(PPM_in[ParamSet.ChannelAssignment[CH_ROLL]] < 70 && PPM_in[ParamSet.ChannelAssignment[CH_NICK]] > 70) setting = 3;
						 // roll stick rightmost and nick stick topmost --> setting 4
						 if(PPM_in[ParamSet.ChannelAssignment[CH_ROLL]] <-70 && PPM_in[ParamSet.ChannelAssignment[CH_NICK]] > 70) setting = 4;
						 // roll stick rightmost and nick stick centered --> setting 5
						 if(PPM_in[ParamSet.ChannelAssignment[CH_ROLL]] <-70 && PPM_in[ParamSet.ChannelAssignment[CH_NICK]] < 70) setting = 5;
						 // update active parameter set in eeprom
						 SetActiveParamSet(setting);
						 ParamSet_ReadFromEEProm(GetActiveParamSet());
						 SetNeutral(false);
						 Beep_Beep(GetActiveParamSet());
					}
					else
					{
						if(ParamSet.GlobalConfig & (CFG_COMPASS_ACTIVE|CFG_GPS_ACTIVE))
						{
							// if roll stick is centered and nick stick is down
							if (abs(PPM_in[ParamSet.ChannelAssignment[CH_ROLL]]) < 30 && PPM_in[ParamSet.ChannelAssignment[CH_NICK]] < -70)
							{
								// nick/roll joystick
								//  _________
								// |         |
								// |         |
								// |         |
								// |         |
								// |    x    |
								//  ¯¯¯¯¯¯¯¯¯
								// enable calibration state of compass
								CompassCalState = 1;
								Beep_SetBeepTime(100);
							}
							else // nick and roll are centered
							{
								ParamSet_ReadFromEEProm(GetActiveParamSet());
								SetNeutral(false);
								Beep_Beep(GetActiveParamSet());
							}
						}
						else // nick and roll are centered
						{
							ParamSet_ReadFromEEProm(GetActiveParamSet());
							SetNeutral(false);
							Beep_Beep(GetActiveParamSet());
						}
					}
				}
			}
			// and if the yaw stick is in the rightmost position
			// save the ACC neutral setting to eeprom
			else if(PPM_in[ParamSet.ChannelAssignment[CH_YAW]] < -75)
			{
				// gas/yaw joystick is top right
				//  _________
				// |        x|
				// |         |
				// |         |
				// |         |
				// |         |
				//  ¯¯¯¯¯¯¯¯¯
				if(++delay_neutral > 200)  // not immediately (wait 200 loops = 200 * 2ms = 0.4 s)
				{
					delay_neutral = 0;
					LED_Green_Set(false);
					ModelIsFlying = 0;
					SetNeutral(true);
					Beep_Beep(GetActiveParamSet());
				}
			}
			else delay_neutral = 0;
		}
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// gas stick is down
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		if(PPM_in[ParamSet.ChannelAssignment[CH_GAS]] < -85)
		{
			if(PPM_in[ParamSet.ChannelAssignment[CH_YAW]] < -75)
			{
				// gas/yaw joystick is bottom right
				//  _________
				// |         |
				// |         |
				// |         |
				// |         |
				// |        x|
				//  ¯¯¯¯¯¯¯¯¯
				// Start Motors
				if(++delay_startmotors > 200) // not immediately (wait 200 loops = 200 * 2ms = 0.4 s)
				{
					delay_startmotors = 200; // do not repeat if once executed
					ModelIsFlying = 1;
					MKFlags |= (MKFLAG_MOTOR_RUN|MKFLAG_START); // set flag RUN and START
					SetPointYaw = 0;
					ReadingIntegralGyroYaw = 0;
					ReadingIntegralGyroPitch = ParamSet.GyroAccFactor * (int32_t)AccPitch;
					ReadingIntegralGyroRoll = ParamSet.GyroAccFactor * (int32_t)AccRoll;
					ReadingIntegralGyroPitch2 = IntegralGyroPitch;
					ReadingIntegralGyroRoll2 = IntegralGyroRoll;
					IPartNick = 0;
					IPartRoll = 0;
				}
			}
			else delay_startmotors = 0; // reset delay timer if sticks are not in this position

			if(PPM_in[ParamSet.ChannelAssignment[CH_YAW]] > 75)
			{
				// gas/yaw joystick is bottom left
				//  _________
				// |         |
				// |         |
				// |         |
				// |         |
				// |x        |
				//  ¯¯¯¯¯¯¯¯¯
				// Stop Motors
				if(++delay_stopmotors > 200)  // not immediately (wait 200 loops = 200 * 2ms = 0.4 s)
				{
					delay_stopmotors = 200; // do not repeat if once executed
					ModelIsFlying = 0;
					MKFlags &= ~(MKFLAG_MOTOR_RUN);
				}
			}
			else delay_stopmotors = 0; // reset delay timer if sticks are not in this position
		}
			// remapping of paameters only if the signal rc-sigbnal conditions are good
	} // eof RC_Quality > 150

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// new values from RC
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	if(!NewPpmData-- || (MKFlags & MKFLAG_EMERGENCY_LANDING)) // NewData = 0 means new data from RC
	{
		static int16_t stick_nick = 0, stick_roll = 0;

		ParameterMapping(); // remapping params (online poti replacement)

		// calculate Stick inputs by rc channels (P) and changing of rc channels (D)
		stick_nick = (stick_nick * 3 + PPM_in[ParamSet.ChannelAssignment[CH_NICK]] * ParamSet.StickP) / 4;
		stick_nick += PPM_diff[ParamSet.ChannelAssignment[CH_NICK]] * ParamSet.StickD;
		StickPitch = stick_nick - GPSStickPitch;

		stick_roll = (stick_roll * 3 + PPM_in[ParamSet.ChannelAssignment[CH_ROLL]] * ParamSet.StickP) / 4;
		stick_roll += PPM_diff[ParamSet.ChannelAssignment[CH_ROLL]] * ParamSet.StickD;
		StickRoll = stick_roll - GPSStickRoll;

		// mapping of yaw
		StickYaw = -PPM_in[ParamSet.ChannelAssignment[CH_YAW]];
		// (range of -2 .. 2 is set to zero, to avoid unwanted yaw trimming on compass correction)
		if(ParamSet.GlobalConfig & (CFG_COMPASS_ACTIVE|CFG_GPS_ACTIVE))
		{
			if (StickYaw > 2) StickYaw-= 2;
			else if (StickYaw< -2) StickYaw += 2;
			else StickYaw = 0;
		}

		// mapping of gas
		StickGas  = PPM_in[ParamSet.ChannelAssignment[CH_GAS]] + 120;// shift to positive numbers

		// update gyro control loop factors
		GyroPFactor = FCParam.GyroP + 10;
		GyroIFactor = FCParam.GyroI;
		GyroYawPFactor = FCParam.GyroP + 10;
		GyroYawIFactor = FCParam.GyroI;


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+ Analog control via serial communication
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		if(ExternControl.Config & 0x01 && FCParam.ExternalControl > 128)
		{
			StickPitch += (int16_t) ExternControl.Nick * (int16_t) ParamSet.StickP;
			StickRoll += (int16_t) ExternControl.Roll * (int16_t) ParamSet.StickP;
			StickYaw += ExternControl.Yaw;
			ExternHeightValue =  (int16_t) ExternControl.Height * (int16_t)ParamSet.Height_Gain;
			if(ExternControl.Gas < StickGas) StickGas = ExternControl.Gas;
		}
		if(StickGas < 0) StickGas = 0;

		// disable I part of gyro control feedback
		if(ParamSet.GlobalConfig & CFG_HEADING_HOLD) GyroIFactor =  0;

		// update max stick positions for nick and roll
		if(abs(StickPitch / STICK_GAIN) > MaxStickPitch)
		{
			MaxStickPitch = abs(StickPitch)/STICK_GAIN;
			if(MaxStickPitch > 100) MaxStickPitch = 100;
		}
		else MaxStickPitch--;
		if(abs(StickRoll / STICK_GAIN) > MaxStickRoll)
		{
			MaxStickRoll = abs(StickRoll)/STICK_GAIN;
			if(MaxStickRoll > 100) MaxStickRoll = 100;
		}
		else MaxStickRoll--;

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Looping?
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		if((PPM_in[ParamSet.ChannelAssignment[CH_ROLL]] > ParamSet.LoopThreshold) && ParamSet.BitConfig & CFG_LOOP_LEFT)  LoopingLeft = 1;
		else
		{
			if(LoopingLeft) // Hysteresis
			{
				if((PPM_in[ParamSet.ChannelAssignment[CH_ROLL]] < (ParamSet.LoopThreshold - ParamSet.LoopHysteresis))) LoopingLeft = 0;
			}
		}
		if((PPM_in[ParamSet.ChannelAssignment[CH_ROLL]] < -ParamSet.LoopThreshold) && ParamSet.BitConfig & CFG_LOOP_RIGHT) LoopingRight = 1;
		else
		{
			if(LoopingRight) // Hysteresis
			{
				if(PPM_in[ParamSet.ChannelAssignment[CH_ROLL]] > -(ParamSet.LoopThreshold - ParamSet.LoopHysteresis)) LoopingRight = 0;
			}
		}

		if((PPM_in[ParamSet.ChannelAssignment[CH_NICK]] > ParamSet.LoopThreshold) && ParamSet.BitConfig & CFG_LOOP_UP) LoopingTop = 1;
		else
		{
			if(LoopingTop) // Hysteresis
			{
				if((PPM_in[ParamSet.ChannelAssignment[CH_NICK]] < (ParamSet.LoopThreshold - ParamSet.LoopHysteresis))) LoopingTop = false;
			}
		}
		if((PPM_in[ParamSet.ChannelAssignment[CH_NICK]] < -ParamSet.LoopThreshold) && ParamSet.BitConfig & CFG_LOOP_DOWN) LoopingDown = true;
		else
		{
			if(LoopingDown) // Hysteresis
			{
				if(PPM_in[ParamSet.ChannelAssignment[CH_NICK]] > -(ParamSet.LoopThreshold - ParamSet.LoopHysteresis)) LoopingDown = false;
			}
		}

		if(LoopingLeft || LoopingRight)  LoopingRoll = true; else LoopingRoll = false;
		if(LoopingTop  || LoopingDown) { LoopingPitch = true; LoopingRoll = false; LoopingLeft = false; LoopingRight = false;} else  LoopingPitch = false;
	} // End of new RC-Values or Emergency Landing


	if(LoopingRoll || LoopingPitch)
	{
		if(GasMixFraction > ParamSet.LoopGasLimit) GasMixFraction = ParamSet.LoopGasLimit;
		FunnelCourse = 1;
	}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// in case of emergency landing
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// set all inputs to save values
	if(MKFlags & MKFLAG_EMERGENCY_LANDING)
	{
		StickYaw = 0;
		StickPitch = 0;
		StickRoll = 0;
		GyroPFactor = 90;
		GyroIFactor = 120;
		GyroYawPFactor = 90;
		GyroYawIFactor = 120;
		LoopingRoll = false;
		LoopingPitch = false;
		MaxStickPitch = 0;
		MaxStickRoll = 0;
	}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Trim Gyro-Integrals to ACC-Signals
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	#define BALANCE_NUMBER 256L
	// sum for averaging
	MeanIntegralGyroPitch  += IntegralGyroPitch;
	MeanIntegralGyroRoll  += IntegralGyroRoll;

	if(LoopingPitch || LoopingRoll) // if looping in any direction
	{
		// reset averaging for acc and gyro integral as well as gyro integral acc correction
		MeasurementCounter = 0;

		MeanAccPitch = 0;
		MeanAccRoll = 0;

		MeanIntegralGyroPitch = 0;
		MeanIntegralGyroRoll = 0;

		ReadingIntegralGyroPitch2 = ReadingIntegralGyroPitch;
		ReadingIntegralGyroRoll2 = ReadingIntegralGyroRoll;

		AttitudeCorrectionPitch = 0;
		AttitudeCorrectionRoll = 0;
	}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	if(!LoopingPitch && !LoopingRoll && ( (AdValueAccZ > 512) || (MKFlags & MKFLAG_MOTOR_RUN)  ) ) // if not lopping in any direction
	{
		int32_t tmp_long, tmp_long2;
		if(FCParam.KalmanK != -1)
		{
			// determine the deviation of gyro integral from averaged acceleration sensor
			tmp_long  = (int32_t)(IntegralGyroPitch / ParamSet.GyroAccFactor - (int32_t)AccPitch);
			tmp_long  = (tmp_long  * FCParam.KalmanK) / (32 * 16);
			tmp_long2 = (int32_t)(IntegralGyroRoll / ParamSet.GyroAccFactor - (int32_t)AccRoll);
			tmp_long2 = (tmp_long2 * FCParam.KalmanK) / (32 * 16);

			if((MaxStickPitch > 64) || (MaxStickRoll > 64)) // reduce effect during stick commands
			{
				tmp_long  /= 2;
				tmp_long2 /= 2;
			}
			if(abs(PPM_in[ParamSet.ChannelAssignment[CH_YAW]]) > 25) // reduce further if yaw stick is active
			{
				tmp_long  /= 3;
				tmp_long2 /= 3;
			}
			// limit correction effect
			if(tmp_long >  (int32_t)FCParam.KalmanMaxFusion)  tmp_long  = (int32_t)FCParam.KalmanMaxFusion;
			if(tmp_long < -(int32_t)FCParam.KalmanMaxFusion)  tmp_long  =-(int32_t)FCParam.KalmanMaxFusion;
			if(tmp_long2 > (int32_t)FCParam.KalmanMaxFusion)  tmp_long2 = (int32_t)FCParam.KalmanMaxFusion;
			if(tmp_long2 <-(int32_t)FCParam.KalmanMaxFusion)  tmp_long2 =-(int32_t)FCParam.KalmanMaxFusion;
		}
		else
		{
			// determine the deviation of gyro integral from acceleration sensor
			tmp_long   = (int32_t)(IntegralGyroPitch / ParamSet.GyroAccFactor - (int32_t)AccPitch);
			tmp_long  /= 16;
			tmp_long2  = (int32_t)(IntegralGyroRoll / ParamSet.GyroAccFactor - (int32_t)AccRoll);
			tmp_long2 /= 16;

			if((MaxStickPitch > 64) || (MaxStickRoll > 64)) // reduce effect during stick commands
			{
				tmp_long  /= 3;
				tmp_long2 /= 3;
			}
			if(abs(PPM_in[ParamSet.ChannelAssignment[CH_YAW]]) > 25) // reduce further if yaw stick is active
			{
				tmp_long  /= 3;
				tmp_long2 /= 3;
			}

			#define BALANCE 32
			// limit correction effect
			EnsureValueInBounds(tmp_long,  -BALANCE, BALANCE);
			EnsureValueInBounds(tmp_long2, -BALANCE, BALANCE);
		}
		// correct current readings
		ReadingIntegralGyroPitch -= tmp_long;
		ReadingIntegralGyroRoll -= tmp_long2;
	}
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// MeasurementCounter is incremented in the isr of analog.c
	if(MeasurementCounter >= BALANCE_NUMBER) // averaging number has reached
	{
		static int16_t cnt = 0;
		static int8_t last_n_p, last_n_n, last_r_p, last_r_n;
		static int32_t MeanIntegralGyroNick_old, MeanIntegralGyroRoll_old;

		// if not lopping in any direction (this should be always the case,
		// because the Measurement counter is reset to 0 if looping in any direction is active.)
		if(!LoopingPitch && !LoopingRoll && !FunnelCourse && ParamSet.DriftComp)
		{
			// Calculate mean value of the gyro integrals
			MeanIntegralGyroPitch /= BALANCE_NUMBER;
			MeanIntegralGyroRoll /= BALANCE_NUMBER;

			// Calculate mean of the acceleration values scaled to the gyro integrals
			MeanAccPitch = (ParamSet.GyroAccFactor * MeanAccPitch) / BALANCE_NUMBER;
			MeanAccRoll = (ParamSet.GyroAccFactor * MeanAccRoll) / BALANCE_NUMBER;

			// Nick ++++++++++++++++++++++++++++++++++++++++++++++++
			// Calculate deviation of the averaged gyro integral and the averaged acceleration integral
			IntegralGyroNickError = (int32_t)(MeanIntegralGyroPitch - (int32_t)MeanAccPitch);
			CorrectionNick = IntegralGyroNickError / ParamSet.GyroAccTrim;
			AttitudeCorrectionPitch = CorrectionNick / BALANCE_NUMBER;
			// Roll ++++++++++++++++++++++++++++++++++++++++++++++++
			// Calculate deviation of the averaged gyro integral and the averaged acceleration integral
			IntegralGyroRollError = (int32_t)(MeanIntegralGyroRoll - (int32_t)MeanAccRoll);
			CorrectionRoll  = IntegralGyroRollError / ParamSet.GyroAccTrim;
			AttitudeCorrectionRoll  = CorrectionRoll  / BALANCE_NUMBER;

			if(((MaxStickPitch > 64) || (MaxStickRoll > 64) || (abs(PPM_in[ParamSet.ChannelAssignment[CH_YAW]]) > 25)) && (FCParam.KalmanK == -1) )
			{
				AttitudeCorrectionPitch /= 2;
				AttitudeCorrectionRoll /= 2;
			}

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Gyro-Drift ermitteln
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			// deviation of gyro nick integral (IntegralGyroPitch is corrected by averaged acc sensor)
			IntegralGyroNickError  = IntegralGyroPitch2 - IntegralGyroPitch;
			ReadingIntegralGyroPitch2 -= IntegralGyroNickError;
			// deviation of gyro nick integral (IntegralGyroPitch is corrected by averaged acc sensor)
			IntegralGyroRollError = IntegralGyroRoll2 - IntegralGyroRoll;
			ReadingIntegralGyroRoll2 -= IntegralGyroRollError;

			if(ParamSet.DriftComp)
			{
				if(YawGyroDrift >  BALANCE_NUMBER/2) AdBiasGyroYaw++;
				if(YawGyroDrift < -BALANCE_NUMBER/2) AdBiasGyroYaw--;
			}
			YawGyroDrift = 0;

			#define ERROR_LIMIT0 (BALANCE_NUMBER / 2)
			#define ERROR_LIMIT1 (BALANCE_NUMBER * 2)
			#define ERROR_LIMIT2 (BALANCE_NUMBER * 16)
			#define MOVEMENT_LIMIT 20000
	// Nick +++++++++++++++++++++++++++++++++++++++++++++++++
			cnt = 1;
			if(IntegralGyroNickError > ERROR_LIMIT1) cnt = 4;
			CorrectionNick = 0;
			if((labs(MeanIntegralGyroNick_old - MeanIntegralGyroPitch) < MOVEMENT_LIMIT) || (FCParam.KalmanMaxDrift > 3 * 8))
			{
				if(IntegralGyroNickError >  ERROR_LIMIT2)
				{
					if(last_n_p)
					{
						cnt += labs(IntegralGyroNickError) / (ERROR_LIMIT2 / 8);
						CorrectionNick = IntegralGyroNickError / 8;
						if(CorrectionNick > 5000) CorrectionNick = 5000;
						AttitudeCorrectionPitch += CorrectionNick / BALANCE_NUMBER;
					}
					else last_n_p = 1;
				}
				else  last_n_p = 0;
				if(IntegralGyroNickError < -ERROR_LIMIT2)
				{
					if(last_n_n)
					{
						cnt += labs(IntegralGyroNickError) / (ERROR_LIMIT2 / 8);
						CorrectionNick = IntegralGyroNickError / 8;
						if(CorrectionNick < -5000) CorrectionNick = -5000;
						AttitudeCorrectionPitch += CorrectionNick / BALANCE_NUMBER;
					}
					else last_n_n = 1;
				}
				else  last_n_n = 0;
			}
			else
			{
				cnt = 0;
				BadCompassHeading = 1000;
			}
			if(cnt > ParamSet.DriftComp) cnt = ParamSet.DriftComp;
			if(FCParam.KalmanMaxDrift) if(cnt > FCParam.KalmanMaxDrift) cnt = FCParam.KalmanMaxDrift;
			// correct Gyro Offsets
			if(IntegralGyroNickError >  ERROR_LIMIT0)   BiasHiResGyroPitch += cnt;
			if(IntegralGyroNickError < -ERROR_LIMIT0)   BiasHiResGyroPitch -= cnt;

	// Roll +++++++++++++++++++++++++++++++++++++++++++++++++
			cnt = 1;
			if(IntegralGyroRollError > ERROR_LIMIT1) cnt = 4;
			CorrectionRoll = 0;
			if((labs(MeanIntegralGyroRoll_old - MeanIntegralGyroRoll) < MOVEMENT_LIMIT) || (FCParam.KalmanMaxDrift > 3 * 8))
			{
				if(IntegralGyroRollError >  ERROR_LIMIT2)
				{
					if(last_r_p)
					{
						cnt += labs(IntegralGyroRollError) / (ERROR_LIMIT2 / 8);
						CorrectionRoll = IntegralGyroRollError / 8;
						if(CorrectionRoll > 5000) CorrectionRoll = 5000;
						AttitudeCorrectionRoll += CorrectionRoll / BALANCE_NUMBER;
					}
					else last_r_p = 1;
				}
				else  last_r_p = 0;
				if(IntegralGyroRollError < -ERROR_LIMIT2)
				{
					if(last_r_n)
					{
						cnt += labs(IntegralGyroRollError) / (ERROR_LIMIT2 / 8);
						CorrectionRoll = IntegralGyroRollError / 8;
						if(CorrectionRoll < -5000) CorrectionRoll = -5000;
						AttitudeCorrectionRoll += CorrectionRoll / BALANCE_NUMBER;
					}
					else last_r_n = 1;
				}
				else  last_r_n = 0;
			}
			else
			{
				cnt = 0;
				BadCompassHeading = 1000;
			}
			// correct Gyro Offsets
			if(cnt > ParamSet.DriftComp) cnt = ParamSet.DriftComp;
			if(FCParam.KalmanMaxDrift) if(cnt > FCParam.KalmanMaxDrift) cnt = FCParam.KalmanMaxDrift;
			if(IntegralGyroRollError >  ERROR_LIMIT0)   BiasHiResGyroRoll += cnt;
			if(IntegralGyroRollError < -ERROR_LIMIT0)   BiasHiResGyroRoll -= cnt;

		}
		else // looping is active
		{
			AttitudeCorrectionRoll  = 0;
			AttitudeCorrectionPitch = 0;
			FunnelCourse = 0;
		}

		// if GyroIFactor == 0 , for example at Heading Hold, ignore attitude correction
		if(!GyroIFactor)
		{
			AttitudeCorrectionRoll  = 0;
			AttitudeCorrectionPitch = 0;
		}
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++
		MeanIntegralGyroNick_old = MeanIntegralGyroPitch;
		MeanIntegralGyroRoll_old = MeanIntegralGyroRoll;
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++
		// reset variables used for next averaging
		MeanAccPitch = 0;
		MeanAccRoll = 0;
		MeanIntegralGyroPitch = 0;
		MeanIntegralGyroRoll = 0;
		MeasurementCounter = 0;
	} // end of averaging


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Yawing
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	if(abs(StickYaw) > 15 ) // yaw stick is activated
	{
		BadCompassHeading = 1000;
		if(!(ParamSet.GlobalConfig & CFG_COMPASS_FIX))
		{
			UpdateCompassCourse = 1;
		}
	}
	// exponential stick sensitivity in yawring rate
	tmp_int  = (int32_t) ParamSet.StickYawP * ((int32_t)StickYaw * abs(StickYaw)) / 512L; // expo  y = ax + bx²
	tmp_int += (ParamSet.StickYawP * StickYaw) / 4;
	SetPointYaw = tmp_int;
	// trimm drift of ReadingIntegralGyroYaw with SetPointYaw(StickYaw)
	ReadingIntegralGyroYaw -= tmp_int;
	// limit the effect
	EnsureValueInBounds(ReadingIntegralGyroYaw, -50000, 50000);

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Compass
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// compass code is used if Compass option is selected
	if(ParamSet.GlobalConfig & (CFG_COMPASS_ACTIVE|CFG_GPS_ACTIVE))
	{
		int16_t w, v, r,correction, error;

		if(CompassCalState && !(MKFlags & MKFLAG_MOTOR_RUN) )
		{
			SetCompassCalState();
#ifdef USE_KILLAGREG
			MM3_Calibrate();
#endif
		}
		else
		{
#ifdef USE_KILLAGREG
			static uint8_t updCompass = 0;
			if (!updCompass--)
			{
				updCompass = 49; // update only at 2ms*50 = 100ms (10Hz)
				MM3_Heading();
			}
#endif

			// get maximum attitude angle
			w = abs(IntegralGyroPitch / 512);
			v = abs(IntegralGyroRoll / 512);
			if(v > w) w = v;
			correction = w / 8 + 1;
			// calculate the deviation of the yaw gyro heading and the compass heading
			if (CompassHeading < 0) error = 0; // disable yaw drift compensation if compass heading is undefined
			else error = ((540 + CompassHeading - (YawGyroHeading / GYRO_DEG_FACTOR)) % 360) - 180;
			if(abs(GyroYaw) > 128) // spinning fast
			{
				error = 0;
			}
			if(!BadCompassHeading && w < 25)
			{
				YawGyroDrift += error;
				if(UpdateCompassCourse)
				{
					Beep_SetBeepTime(20);
					YawGyroHeading = (int32_t)CompassHeading * GYRO_DEG_FACTOR;
					CompassCourse = (int16_t)(YawGyroHeading / GYRO_DEG_FACTOR);
					UpdateCompassCourse = 0;
				}
			}
			YawGyroHeading += (error * 8) / correction;
			w = (w * FCParam.CompassYawEffect) / 32;
			w = FCParam.CompassYawEffect - w;
			if(w >= 0)
			{
				if(!BadCompassHeading)
				{
					v = 64 + (MaxStickPitch + MaxStickRoll) / 8;
					// calc course deviation
					r = ((540 + (YawGyroHeading / GYRO_DEG_FACTOR) - CompassCourse) % 360) - 180;
					v = (r * w) / v; // align to compass course
					// limit yaw rate
					w = 3 * FCParam.CompassYawEffect;
					if (v > w) v = w;
					else if (v < -w) v = -w;
					ReadingIntegralGyroYaw += v;
				}
				else
				{ // wait a while
					BadCompassHeading--;
				}
			}
			else
			{ // ignore compass at extreme attitudes for a while
				BadCompassHeading = 500;
			}
		}
	}

#if (defined (USE_KILLAGREG) || defined (USE_MK3MAG))
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  GPS
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	if(ParamSet.GlobalConfig & CFG_GPS_ACTIVE)
	{
		GPS_Main();
		MKFlags &= ~(MKFLAG_CALIBRATE | MKFLAG_START);
	}
	else
	{
		GPSStickPitch = 0;
		GPSStickRoll = 0;
	}
#endif

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Debugwerte zuordnen
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	if(!TimerDebugOut--)
	{
		TimerDebugOut = 24; // update debug outputs every 25*2ms = 50 ms (20Hz)
		DebugOut.Analog[0]  = (10 * IntegralGyroPitch) / GYRO_DEG_FACTOR; // in 0.1 deg
		DebugOut.Analog[1]  = (10 * IntegralGyroRoll) / GYRO_DEG_FACTOR; // in 0.1 deg
		DebugOut.Analog[2]  = (10 * AccPitch) / ACC_DEG_FACTOR; // in 0.1 deg
		DebugOut.Analog[3]  = (10 * AccRoll) / ACC_DEG_FACTOR; // in 0.1 deg
		DebugOut.Analog[4]  = GyroYaw;
		DebugOut.Analog[5]  = ReadingHeight;
		DebugOut.Analog[6]  = (ReadingIntegralTop / 512);
		DebugOut.Analog[8]  = CompassHeading;
		DebugOut.Analog[9]  = UBat;
		DebugOut.Analog[10] = RC_Quality;
		DebugOut.Analog[11] = YawGyroHeading / GYRO_DEG_FACTOR;
		DebugOut.Analog[19] = CompassCalState;
	//	DebugOut.Analog[24] = GyroPitch/2;
	//	DebugOut.Analog[25] = GyroRoll/2;
		DebugOut.Analog[27] = (int16_t)FCParam.KalmanMaxDrift;
	//	DebugOut.Analog[28] = (int16_t)FCParam.KalmanMaxFusion;
		DebugOut.Analog[30] = GPSStickPitch;
		DebugOut.Analog[31] = GPSStickRoll;
	}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  calculate control feedback from angle (gyro integral) and agular velocity (gyro signal)
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	#define TRIM_LIMIT 200
	EnsureValueInBounds(TrimPitch, -TRIM_LIMIT, TRIM_LIMIT);
	EnsureValueInBounds(TrimRoll, -TRIM_LIMIT, TRIM_LIMIT);

	if(FunnelCourse) {
		IPartNick = 0;
		IPartRoll = 0;
	}

	if(!LoopingPitch)
	{
		PPartNick = (IntegralGyroPitch * GyroIFactor) / (44000 / STICK_GAIN); // P-Part
	}
	else
	{
		PPartNick = 0;
	}
	PDPartNick = PPartNick + (int32_t)((int32_t)GyroPitch * GyroPFactor + (int32_t)TrimPitch * 128L) / (256L / STICK_GAIN); //  +D-Part

	if(!LoopingRoll)
	{
		PPartRoll = (IntegralGyroRoll * GyroIFactor) / (44000 / STICK_GAIN); // P-Part
	}
	else
	{
		PPartRoll = 0;
	}
	PDPartRoll = PPartRoll + (int32_t)((int32_t)GyroRoll * GyroPFactor +  (int32_t)TrimRoll * 128L) / (256L / STICK_GAIN); // +D-Part

	PDPartYaw =  (int32_t)(GyroYaw * 2 * (int32_t)GyroYawPFactor) / (256L / STICK_GAIN) + (int32_t)(IntegralGyroYaw * GyroYawIFactor) / (2 * (44000 / STICK_GAIN));

	//DebugOut.Analog[21] = PDPartNick;
	//DebugOut.Analog[22] = PDPartRoll;

	// limit control feedback
	#define SENSOR_LIMIT  (4096 * 4)
	EnsureValueInBounds(PDPartNick, -SENSOR_LIMIT, SENSOR_LIMIT);
	EnsureValueInBounds(PDPartRoll, -SENSOR_LIMIT, SENSOR_LIMIT);
	EnsureValueInBounds(PDPartYaw,  -SENSOR_LIMIT, SENSOR_LIMIT);

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// all BL-Ctrl connected?
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	if(MissingMotor) {
		// if we are in the lift off condition
		if( (ModelIsFlying > 1) && (ModelIsFlying < 50) && (GasMixFraction > 0) )
		ModelIsFlying = 1; // keep within lift off condition
		GasMixFraction = ParamSet.GasMin; // reduce gas to min to avoid lift of
	}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Height Control
// The height control algorithm reduces the gas but does not increase the gas.
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	GasMixFraction *= STICK_GAIN;

	// if height control is activated and no emergency landing is active
	if((ParamSet.GlobalConfig & CFG_HEIGHT_CONTROL) && !(MKFlags & MKFLAG_EMERGENCY_LANDING) )
	{
		int tmp_int;
		static uint8_t delay = 100;
		// if height control is activated by an rc channel
		if(ParamSet.GlobalConfig & CFG_HEIGHT_SWITCH)
		{ // check if parameter is less than activation threshold
			if(
				( (ParamSet.BitConfig & CFG_HEIGHT_3SWITCH) && ( (FCParam.MaxHeight > 80) && (FCParam.MaxHeight < 140) ) )|| // for 3-state switch height control is only disabled in center position
				(!(ParamSet.BitConfig & CFG_HEIGHT_3SWITCH) && (FCParam.MaxHeight < 50) ) // for 2-State switch height control is disabled in lower position
			)
			{ //hight control not active
				if(!delay--)
				{
					// measurement of air pressure close to upper limit and no overflow in correction of the new OCR0A value occurs
					if( (ReadingAirPressure > 1000) && (OCR0A < 255) )
					{ // increase offset
						if(OCR0A < 244)
						{
							ExpandBaro -= 10;
							OCR0A = PressureSensorOffset - ExpandBaro;
						}
						else
						{
							OCR0A = 254;
						}
						Beep_SetBeepTime(30);
						delay = 250;
					}
					// measurement of air pressure close to lower limit and
					else if( (ReadingAirPressure < 100) && (OCR0A > 1) )
					{ // decrease offset
						if(OCR0A > 10)
						{
							ExpandBaro += 10;
							OCR0A = PressureSensorOffset - ExpandBaro;
						}
						else
						{
							OCR0A = 1;
						}
						Beep_SetBeepTime(30);
						delay = 250;
					}
					else
					{
						SetPointHeight = ReadingHeight - 20;  // update Setpoint with current reading
						HeightControlActive = 0; // disable height control
						delay = 1;
					}
				}
			}
			else
			{ //hight control not active
				HeightControlActive = 1; // enable height control
				delay = 200;
			}
		}
		else // no switchable height control
		{
			SetPointHeight = ((int16_t) ExternHeightValue + (int16_t) FCParam.MaxHeight) * (int16_t)ParamSet.Height_Gain - 20;
			HeightControlActive = 1;
		}
		// get current height
		h = ReadingHeight;
		// if current height is above the setpoint reduce gas
		if((h > SetPointHeight) && HeightControlActive)
		{
			// height difference -> P control part
			h = ((h - SetPointHeight) * (int16_t) FCParam.HeightP) / (16 / STICK_GAIN);
			h = GasMixFraction - h; // reduce gas
			// height gradient --> D control part
			//h -= (HeightD * FCParam.HeightD) / (8 / STICK_GAIN);  // D control part
			h -= (HeightD) / (8 / STICK_GAIN);  // D control part
			// acceleration sensor effect
			tmp_int = ((ReadingIntegralTop / 128) * (int32_t) FCParam.Height_ACC_Effect) / (128 / STICK_GAIN);
			if(tmp_int > 70 * STICK_GAIN)        tmp_int =   70 * STICK_GAIN;
			else if(tmp_int < -(70 * STICK_GAIN)) tmp_int = -(70 * STICK_GAIN);
			h -= tmp_int;
			// update height control gas
			HeightControlGas = (HeightControlGas*15 + h) / 16;
			// limit gas reduction
			if(HeightControlGas < ParamSet.HeightMinGas * STICK_GAIN) {
				if(GasMixFraction >= ParamSet.HeightMinGas * STICK_GAIN) HeightControlGas = ParamSet.HeightMinGas * STICK_GAIN;
				// allows landing also if gas stick is reduced below min gas on height control
				if(GasMixFraction < ParamSet.HeightMinGas * STICK_GAIN) HeightControlGas = GasMixFraction;
			}
			// limit gas to stick setting
			if(HeightControlGas > GasMixFraction) HeightControlGas = GasMixFraction;
			GasMixFraction = HeightControlGas;
		}
	}
	// limit gas to parameter setting
	if(GasMixFraction > (ParamSet.GasMax - 20) * STICK_GAIN) GasMixFraction = (ParamSet.GasMax - 20) * STICK_GAIN;
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// + Mixer and PI-Controller
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	DebugOut.Analog[7] = GasMixFraction;

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Yaw-Fraction
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	YawMixFraction = PDPartYaw - SetPointYaw * STICK_GAIN;     // yaw controller
	#define MIN_YAWGAS (40 * STICK_GAIN)  // yaw also below this gas value
	// limit YawMixFraction
	if(GasMixFraction > MIN_YAWGAS) {
		EnsureValueInBounds(YawMixFraction, -(GasMixFraction / 2), (GasMixFraction / 2));
	} else {
		EnsureValueInBounds(YawMixFraction, -(MIN_YAWGAS / 2), (MIN_YAWGAS / 2));
	}
	tmp_int = ParamSet.GasMax * STICK_GAIN;
	EnsureValueInBounds(YawMixFraction, -(tmp_int - GasMixFraction), (tmp_int - GasMixFraction));

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Nick-Axis
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	DiffNick = PDPartNick - StickPitch;	// get difference
	if(GyroIFactor) IPartNick += PPartNick - StickPitch; // I-part for attitude control
	else IPartNick += DiffNick; // I-part for head holding
	EnsureValueInBounds(IPartNick, -(STICK_GAIN * 16000L), (STICK_GAIN * 16000L));
	PitchMixFraction = DiffNick + (IPartNick / Ki); // PID-controller for nick

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Roll-Axis
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	DiffRoll = PDPartRoll - StickRoll;	// get difference
	if(GyroIFactor) IPartRoll += PPartRoll - StickRoll; // I-part for attitude control
	else IPartRoll += DiffRoll;  // I-part for head holding
	EnsureValueInBounds(IPartRoll, -(STICK_GAIN * 16000L), (STICK_GAIN * 16000L));
	RollMixFraction = DiffRoll + (IPartRoll / Ki);	 // PID-controller for roll

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Limiter
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	tmp_int = (int32_t)((int32_t)FCParam.DynamicStability * (int32_t)(GasMixFraction + abs(YawMixFraction) / 2)) / 64;
	EnsureValueInBounds(PitchMixFraction, -tmp_int, tmp_int);
	EnsureValueInBounds(RollMixFraction, -tmp_int, tmp_int);

	// universal mixer
	Motor_Mixer(GasMixFraction, PitchMixFraction, RollMixFraction, YawMixFraction);
}
