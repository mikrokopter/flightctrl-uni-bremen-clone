
#include "beep.h"

#include <avr/io.h>
#include "fc.h"
#include "main.h"
#include "timer0.h"

volatile uint16_t BeepModulation = 0xFFFF;
volatile uint16_t BeepTime = 0;

void Beep_Beep(uint8_t times)
{
	while(times--)
	{
		// do nothing if motors are running
		if(MKFlags & MKFLAG_MOTOR_RUN) {
			return;
		}

		Beep_SetBeepTime(100); // beeps last 100ms
		Timer_WaitFor(250); // blocks 250ms as pause to next beep.
	}
}

inline bool Beep_IsBeeping(void)
{
	return BeepTime != 0;
}

void Beep_Set(bool on)
{
	if(on)
	{ // set speaker port to high
		if(BoardRelease == 10) PORTD |= _BV(PORTD2); // Speaker at PD2
		else                   PORTC |= _BV(PORTC7); // Speaker at PC7
	}
	else
	{ // set speaker port to low
		if(BoardRelease == 10) PORTD &= ~_BV(PORTD2); // Speaker at PD2
		else                   PORTC &= ~_BV(PORTC7); // Speaker at PC7
	}
}


// fields


inline void Beep_SetBeepTime(uint16_t ms)
{
	BeepTime = ms*10;
}
