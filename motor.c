
#include "motor.h"

#include "eeprom.h"
#include "fc.h"
#include "twimaster.h"


MixerTable_t Mixer;
MotorData_t  Motor[Motor_Count];


void Motor_Mixer(uint16_t gasMixFraction, uint16_t pitchMixFraction, uint16_t rollMixFraction, uint16_t yawMixFraction)
{
	// high resolution motor values for smoothing of PID motor outputs
	static int16_t oldMotorValue[Motor_Count];

	uint8_t i;
	for(i = 0; i < Motor_Count; i++) {
		// the new motor value to be calculated
		int16_t newMotorValue;

		// only use mixer if there is gas
		if(Mixer.Motor[i][MIX_GAS] > 0) {
			newMotorValue =  ((int32_t)  gasMixFraction * Mixer.Motor[i][MIX_GAS] ) / MIXER_MAX;
			newMotorValue += ((int32_t)pitchMixFraction * Mixer.Motor[i][MIX_NICK]) / MIXER_MAX;
			newMotorValue += ((int32_t) rollMixFraction * Mixer.Motor[i][MIX_ROLL]) / MIXER_MAX;
			newMotorValue += ((int32_t)  yawMixFraction * Mixer.Motor[i][MIX_YAW] ) / MIXER_MAX;

			// spike filter
			newMotorValue = Motor_SmoothValues(oldMotorValue[i], newMotorValue);
			// remember motor value for next time
			oldMotorValue[i] = newMotorValue;

			// revert stick gain and do bounds check
			newMotorValue /= STICK_GAIN;
			EnsureValueInBounds(newMotorValue, ParamSet.GasMin, ParamSet.GasMax);

			// update setpoint to be sent to BL
			Motor[i].Setpoint = newMotorValue;
		} else {
			Motor[i].Setpoint = 0;
		}
	}
}

int16_t Motor_SmoothValues(int16_t oldValue, int16_t newValue)
{
	//! \todo why ist this calculation done this way?
	if(newValue > oldValue) return (1 * (int16_t)oldValue + newValue) / 2; //mean of old and new
	else                    return newValue - (oldValue - newValue) * 1; // 2 * new - old
}
