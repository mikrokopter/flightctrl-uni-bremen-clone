#ifndef _RC_H
#define _RC_H

#include <inttypes.h>

#define J3HIGH    PORTD |= (1<<PORTD5)
#define J3LOW     PORTD &= ~(1<<PORTD5)
#define J3TOGGLE  PORTD ^= (1<<PORTD5)

#define J4HIGH    PORTD |= (1<<PORTD4)
#define J4LOW     PORTD &= ~(1<<PORTD4)
#define J4TOGGLE  PORTD ^= (1<<PORTD4)

#define J5HIGH    PORTD |= (1<<PORTD3)
#define J5LOW     PORTD &= ~(1<<PORTD3)
#define J5TOGGLE  PORTD ^= (1<<PORTD3)


extern void RC_Init (void);
extern volatile int16_t PPM_in[15];		// the RC-Signal
extern volatile int16_t PPM_diff[15];	// the differentiated RC-Signal
extern volatile uint8_t NewPpmData;     // 0 indicates a new recieved PPM Frame
extern volatile int16_t RC_Quality;     // rc signal quality indicator (0 to 200)

#endif //_RC_H
