/*!
	\file
	\brief Contains the entry point for the FlightCtrl.

	Here the FlightCtrl sets up its components and enters the main loop.
*/

#ifndef _MAIN_H
#define _MAIN_H


#ifndef F_CPU // if it has not been set in the Makefile
	//! The controller's system clock/CPU frequency in Hz.
	#define F_CPU  20000000
#endif

//! The controller's system clock/CPU frequency in Hz.
#define SYSCLK  F_CPU

/*!
	Defines the FlightCtrl board revision.
	\todo make this a compile time destinction
	\todo link to different board versions/explanations
*/
extern uint8_t BoardRelease;

//! The list of supported CPUs.
typedef enum {ATMEGA644, ATMEGA644P} CPUType_t;
/*!
	The type of CPU on the FlightCtrl.
	\note This is automatically determined when the FlightCtrl starts.
	\todo make this a compile time destinction
	\todo Add explenation why the destinction is necessary.
*/
extern CPUType_t CPUType;

/*!
	\brief Initializes all subsystems before entering the main event loop.
	\warning Do not call this form your own code.
*/
void Main_Init(void);

/*!
	\brief Prints initial status information onto the LCD.
	\todo Check for initalization code and move it to Main_Init().
	\todo Make this only be called when a LCD is present.
*/
void Main_Print(void);

/*!
	\brief Containts the main event loop of the FlightCtrl.
	\warning Do not call this form your own code.
*/
void Main_Loop(void);

#endif //_MAIN_H
