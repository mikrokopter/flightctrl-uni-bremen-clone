/*!
	\file
	\brief Contains functions and variables to control the beeping of the speaker.
*/

#ifndef _BEEP_H
#define _BEEP_H

#include <inttypes.h>
#include <stdbool.h>

/*!
	\brief Modulates a beep according to the set pattern.

	A tone will come out of the speaker if the following condition is true.
	\code (BeepTime & BeepModulation) != 0 \endcode
	A pattern of \c 0xFFFF represents a continuous beep during the whole of \ref BeepTime.
*/
extern volatile uint16_t BeepModulation;

/*!
	\brief Holds a counter for how long a beep should last.

	\note This has a resolution of ~0.1ms and is decreased in the \link ISR(TIMER0_OVF_vect) ISR of Timer0\endlink.
	\warning Do not use this variable directly.
	\sa Beep_SetBeepTime(), ISR(TIMER0_OVF_vect)
*/
extern volatile uint16_t BeepTime;

/*!
	\brief Beeps for a given number of times.

	Each beep lasts 10ms within a following pause of 240ms.
	So the total time spent in here will be \a times * 250ms.

	\param times The number of times to beep.
	\warning This will block untill all beeps are carried out.
					 Use a combination of \ref BeepTime and \ref BeepModulation to achieve multiple non-blocking beeps.
	\note This will do nothing (i.e. return immediately) if the motors are running.
	\sa MKFLAG_MOTOR_RUN
*/
void Beep_Beep(uint8_t times);

/*!
	\brief Check if there is a beep beeing carried out.

	\return \c true if there is a beep (or even a sequence of beeps) beeing carried out, otherwise \c false.
	\sa BeepTime
*/
bool Beep_IsBeeping(void);

/*!
	\brief Controls the beeping sound of the speaker.

	\param on Whether the beep should be turned on or off.
*/
void Beep_Set(bool on);


// fields


/*!
	\brief Sets the beeper to beep for a given time.

	\param ms Time to beep in milliseconds.
*/
void Beep_SetBeepTime(uint16_t ms);

#endif //_BEEP_H
