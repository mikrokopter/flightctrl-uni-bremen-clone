/*!
	\file
	Contains the Flight-Control's main code.
*/

#ifndef _FC_H
#define _FC_H

#include <inttypes.h>
#include <stdbool.h>

/*!
	\brief Specifies the scaling factor for \ref AccPitch and \ref AccRoll to have a better resolution.

	Example: \code AccPitch = ACC_AMPLIFY * AdValueAccPitch \endcode
*/
#define ACC_AMPLIFY  6

/*!
	\brief Specifies the factor used for converting \ref AccPitch and \ref AccRoll to hteir approximate values in degrees.

	Example: \code PitchAngle = AccPitch / ACC_DEG_FACTOR \endcode

	The value is derived from the datasheet of the acceleration sensor where 5g are scaled to \f$ V_{Ref} \f$ (which are converted to 10 Bit values or 1024 counts).
	Therefore 1g is equal to \f$ 1024/5 = 205 \f$ counts.
	The \link ISR(ADC_vect) ADC ISR \endlink sums 2 accelerator samples to AdValueAcc
	and 1g yields to \f$ AdValueAcc = 2 * 205 = 410 \f$ which is again scaled by \ref ACC_AMPLIFY
	resulting in 1g being equal to \f$ Acc = 2 * 205 * 6 = 2460 \f$.
	The linear approx of the \f$ arcsin \f$ and the scaling of Acc gives \f$ sin(20^\circ) * 2460 = 841 \f$ and \f$ 841 / 20 = 42 \f$
*/
#define ACC_DEG_FACTOR  42

/*!
	\brief Specifies the factor for converting IntegralGyroPitch, IntegralGyroRoll and IntegralGyroYaw to their approximate values in degrees.

	Example: \code PitchAngle = IntegralGyroPitch / GYRO_DEG_FACTOR \endcode

	\sa ACC_DEG_FACTOR
*/
#define GYRO_DEG_FACTOR  ((int16_t)(ParamSet.GyroAccFactor) * ACC_DEG_FACTOR)

//! Increases the resolution of setpoints.
#define STICK_GAIN 4

/*!
	Ensures that a given value is between the given bounds and limits it to those bounds when they are exceeded.
	\param value The value to be checked.
	\param min The lower bound.
	\param max The upper bound.
*/
#define EnsureValueInBounds(value, min, max) \
{ \
	if(value < min) { \
		value = min; \
	} else if(value > max) { \
		value = max; \
	} \
}

typedef struct
{
	uint8_t HeightD;
	uint8_t MaxHeight;
	uint8_t HeightP;
	uint8_t Height_ACC_Effect;
	uint8_t CompassYawEffect;
	uint8_t GyroD;
	uint8_t GyroP;
	uint8_t GyroI;
	uint8_t StickYawP;
	uint8_t IFactor;
	uint8_t UserParam1;
	uint8_t UserParam2;
	uint8_t UserParam3;
	uint8_t UserParam4;
	uint8_t UserParam5;
	uint8_t UserParam6;
	uint8_t UserParam7;
	uint8_t UserParam8;
	uint8_t ServoNickControl;
	uint8_t LoopGasLimit;
	uint8_t AxisCoupling1;
	uint8_t AxisCoupling2;
	uint8_t AxisCouplingYawCorrection;
	uint8_t DynamicStability;
	uint8_t ExternalControl;
	uint8_t J16Timing;
	uint8_t J17Timing;
#if (defined (USE_KILLAGREG) || defined (USE_MK3MAG))
	uint8_t NaviGpsModeControl;
	uint8_t NaviGpsGain;
	uint8_t NaviGpsP;
	uint8_t NaviGpsI;
	uint8_t NaviGpsD;
	uint8_t NaviGpsACC;
	uint8_t NaviOperatingRadius;
	uint8_t NaviWindCorrection;
	uint8_t NaviSpeedCompensation;
#endif
	int8_t KalmanK;
	int8_t KalmanMaxDrift;
	int8_t KalmanMaxFusion;
} fc_param_t;

extern fc_param_t FCParam;

/*!
	Gyroscope pitch rotation rate reading.
	\todo Determine value range.
*/
extern int16_t GyroPitch;
/*!
	Gyroscope roll rotation rate reading.
	\todo Determine value range.
*/
extern int16_t GyroRoll;
/*!
	Gyroscope yaw rotation rate reading.
	\todo Determine value range.
*/
extern int16_t GyroYaw;

extern int32_t IntegralGyroPitch; //!< Pitch attitude calcualted by temporal integral of gyro rates.
extern int32_t IntegralGyroRoll; //!< Roll attitude calcualted by temporal integral of gyro rates.
extern int32_t IntegralGyroYaw; //!< Yaw attitude calcualted by temporal integral of gyro rates.

// attitude acceleration integrals
extern int32_t MeanAccPitch; //!< Pitch acceleration calcualted by temporal integral of accelerator rates.
extern int32_t MeanAccRoll; //!< Roll acceleration calcualted by temporal integral of accelerator rates.
extern volatile int32_t ReadingIntegralTop; //!< calculated in analog.c

// bias values
extern int16_t BiasHiResGyroPitch; //!< Gyro pitch bias value.
extern int16_t BiasHiResGyroRoll; //!< Gyro roll bias value.
extern int16_t AdBiasGyroYaw;
extern int16_t AdBiasAccPitch;
extern int16_t AdBiasAccRoll;
extern volatile float AdBiasAccTop;

/*!
	Holds the current height.
*/
extern int ReadingHeight;
/*!
	Holds the set height to be held by the height control.
	\todo why are there \code SetPointHeight = ... - 20; \endcode when setting this
*/
extern int SetPointHeight;

/*!
	Pitch acceleration value.
	\todo Determine value range.
*/
extern int16_t AccPitch;
/*!
	Roll acceleration value.
	\todo Determine value range.
*/
extern int16_t AccRoll;
/*!
	Top acceleration value.
	\todo Determine value range.
*/
extern int16_t AccTop;

/*!
	Pitch acceleration to be sent to navi board.
	It is the accumulation of multiple samples.
	This is reset after the package to be sent to the navi board is assembled.
	\sa NaviCntAcc
*/
extern int16_t NaviAccPitch;
/*!
	Roll acceleration to be sent to navi board.
	It is the accumulation of multiple samples.
	This is reset after the package to be sent to the navi board is assembled.
	\sa NaviCntAcc
*/
extern int16_t NaviAccRoll;
/*!
	Holds account of how many samples have been accumulated onto NaviAccPitch and NaviAccRoll.
	This is reset after the package to be sent to the navi board is assembled.
*/
extern int16_t NaviCntAcc;

extern int16_t TrimPitch; //!< Additive pitch gyro rate corrections
extern int16_t TrimRoll; //!< Additive roll gyro rate corrections

extern long TurnOver180Pitch; //!< Pitch looping param
extern long TurnOver180Roll; //!< Roll looping param

// attitude gyro integrals
extern int32_t IntegralGyroPitch;
extern int32_t IntegralGyroPitch2;
extern int32_t IntegralGyroRoll;
extern int32_t IntegralGyroRoll2;
extern int32_t IntegralGyroYaw;
extern int32_t ReadingIntegralGyroPitch;
extern int32_t ReadingIntegralGyroPitch2;
extern int32_t ReadingIntegralGyroRoll;
extern int32_t ReadingIntegralGyroRoll2;
extern int32_t ReadingIntegralGyroYaw;
extern int32_t MeanIntegralGyroPitch;
extern int32_t MeanIntegralGyroRoll;

// compass course
extern int16_t CompassHeading; //!< negative angle indicates invalid data.
extern int16_t CompassCourse;
/*!
	\code CompassOffCourse = ((540 + CompassHeading - CompassCourse) % 360) - 180 \endcode
	\todo Can probably be removed, as it is only set but never read.
*/
extern int16_t CompassOffCourse;
extern uint8_t CompassCalState;
extern uint8_t FunnelCourse;
extern uint16_t BadCompassHeading;
extern int32_t YawGyroHeading; //!< Yaw Gyro Integral supported by compass
extern int16_t YawGyroDrift;

// the PD factors for the attitude control
extern uint8_t GyroPFactor;
extern uint8_t GyroIFactor;
// the PD factors for the yae control
extern uint8_t GyroYawPFactor;
extern uint8_t GyroYawIFactor;

/*!
	Used in pitch and roll PID controllers.
	This is calculated by assigning \code 10300 / (FCParam.IFactor + 1) \endcode
	\todo where does the 10300 come from?
*/
extern int16_t Ki;

extern int16_t Poti1;
extern int16_t Poti2;
extern int16_t Poti3;
extern int16_t Poti4;
extern int16_t Poti5;
extern int16_t Poti6;
extern int16_t Poti7;
extern int16_t Poti8;

extern int16_t ExternHeightValue; //!< Height value from UART

extern int16_t AttitudeCorrectionRoll;
extern int16_t AttitudeCorrectionPitch;

extern bool LoopingPitch; //!< Indication whether the MK is in a looping on the pitch axis
extern bool LoopingRoll; //!< Indication whether the MK is in a looping on the roll axis
extern bool LoopingLeft;
extern bool LoopingRight;
extern bool LoopingDown;
extern bool LoopingTop;

/*!
	Averaging Measurement Readings
*/
void Mean(void);

/*!
	MotorControl
*/
void MotorControl(void);

/*!
	Map the parameter to poti values.
*/
void ParameterMapping(void);

/*!
	Transmit motor data via TWI.
*/
void SendMotorData(void);

/*!
*/
void SetCompassCalState(void);

/*!
	Establisch neutral readings.
	\param DoAccAdjustment Whether to recalculate the accelerator biases.
	\sa SetNeutral_RestoreAccBias(), SetNeutral_SaveAccBias()
*/
void SetNeutral(bool DoAccAdjustment);

/*!
	Restores the accelerator bias values from EEPROM.
*/
void SetNeutral_RestoreAccBias(void);

/*!
	Calculates new accelertor bias values and saves them to EEPROM.
*/
void SetNeutral_SaveAccBias(void);

extern int16_t Poti1;
extern int16_t Poti2;
extern int16_t Poti3;
extern int16_t Poti4;
extern int16_t Poti5;
extern int16_t Poti6;
extern int16_t Poti7;
extern int16_t Poti8;

// current stick values
extern int16_t StickPitch;
extern int16_t StickRoll;
extern int16_t StickYaw;
// current GPS-stick values
extern int16_t GPSStickPitch;
extern int16_t GPSStickRoll;

// current stick elongations
extern int16_t MaxStickPitch;
extern int16_t MaxStickRoll;
extern int16_t MaxStickYaw;


extern uint16_t ModelIsFlying;


// MKFlags
#define MKFLAG_MOTOR_RUN  0x01 //!< MK flag set when motors are running
#define MKFLAG_FLY        0x02 //!< MK flag set when the copter is believed to be flying
#define MKFLAG_CALIBRATE  0x04
#define MKFLAG_START      0x08
#define MKFLAG_EMERGENCY_LANDING  0x10 //!< MK flag set when the copter is performing an emergency landing
#define MKFLAG_RESERVE1   0x20
#define MKFLAG_RESERVE2   0x40
#define MKFLAG_RESERVE3   0x80

/*!
	Informs about the current MK state.
*/
extern volatile uint8_t MKFlags;

#endif //_FC_H

